﻿using MySql.Data.MySqlClient;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Base;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras;
using Nsf.PuroTempero.MODELO.Módulo_Compras;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.PuroTempero.DATABASE.Módulo_Compras
{
    public class Database_CompraItem
    {
        public int Salvar (DTO_CompraItem dto)
        {
            string script =
            @"INSERT INTO tb_compra_item 
            (
                id_compra,
                id_produto
            )
            VALUES
            (
                @id_compra,
                @id_produto
            )";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_compra", dto.ID_Compra));
            parm.Add(new MySqlParameter("id_produto", dto.ID_Produto));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parm);
        }
    }
}
