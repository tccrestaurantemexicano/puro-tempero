﻿using MySql.Data.MySqlClient;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Base;
using Nsf.PuroTempero.MODELO.Módulo_Estoque;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.PuroTempero.DATABASE.Módulo_Estoque
{
    public class VIEW_EstoqueDatabase
    {
        public List<VIEW_EstoqueDTO> Consultar(int id)
        {
            string script = @"SELECT * FROM view_consultar_estoque
                                      WHERE id_compra = @id_compra";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_compra", id));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<VIEW_EstoqueDTO> fora = new List<VIEW_EstoqueDTO>();

            while (reader.Read())
            {
                VIEW_EstoqueDTO dentro = new VIEW_EstoqueDTO();
                dentro.ID = reader.GetInt32("id_compra");
                dentro.ID_Produto = reader.GetInt32("id_produto");
                dentro.Fornecedor = reader.GetString("ds_razao_social");
                dentro.Produto = reader.GetString("nm_produto");
                dentro.Marca = reader.GetString("nm_marca");
                dentro.Preco = reader.GetDecimal("vl_preco");
                dentro.Quantidade = reader.GetInt32("qtd_itens");
                dentro.Total = reader.GetDecimal("vl_total");

                fora.Add(dentro);
            }
            reader.Close();
            return fora;
        }

        public List<VIEW_EstoqueDTO> Listar ()
        {
            string script = @"SELECT * FROM view_consultar_estoque";

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);
            List<VIEW_EstoqueDTO> fora = new List<VIEW_EstoqueDTO>();

            while (reader.Read())
            {
                VIEW_EstoqueDTO dentro = new VIEW_EstoqueDTO();
                dentro.ID = reader.GetInt32("id_compra");
                dentro.ID_Produto = reader.GetInt32("id_produto");
                dentro.Fornecedor = reader.GetString("ds_razao_social");
                dentro.Produto = reader.GetString("nm_produto");
                dentro.Marca = reader.GetString("nm_marca");
                dentro.Preco = reader.GetDecimal("vl_preco");
                dentro.Quantidade = reader.GetInt32("qtd_itens");
                dentro.Total = reader.GetDecimal("vl_total");

                fora.Add(dentro);
            }
            reader.Close();
            return fora;
        }
    }
}
