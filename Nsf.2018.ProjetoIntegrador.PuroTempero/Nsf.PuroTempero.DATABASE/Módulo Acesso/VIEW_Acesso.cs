﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_de_Acesso.Database
{
    public class VIEW_Acesso
    {
        public VIEW_AcessoDTO Logar(VIEW_AcessoDTO dto)
        {
            Base.Database db = new Base.Database();

            string script = @"SELECT * FROM view_acesso_funcionario
                                      WHERE ds_usuario =@ds_usuario
                                      AND   ds_senha   =@ds_senha";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("ds_usuario", dto.Usuario));
            parm.Add(new MySqlParameter("ds_senha", dto.Senha));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            VIEW_AcessoDTO fora = null;

            if (reader.Read())
            {
                fora = new VIEW_AcessoDTO();
                fora.ID = reader.GetInt32("id_nivel_acesso");
                fora.ID_Funcionario = reader.GetInt32("id_funcionario");
                fora.ID_Departamento = reader.GetInt32("id_departamento");
                fora.Usuario = reader.GetString("ds_usuario");
                fora.Senha = reader.GetString("ds_senha");

                fora.Alterar = reader.GetBoolean("bl_alterar");
                fora.Salvar = reader.GetBoolean("bl_salvar");
                fora.Remover = reader.GetBoolean("bl_remover");
                fora.Consultar = reader.GetBoolean("bl_consultar");
            }

            reader.Close();
            return fora;
        }
    }
}
