﻿using MySql.Data.MySqlClient;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_Financeiro
{
    public class VIEW_FinanceiroDatabase
    {
        public List<VIEW_FinanceiroDTO> Consultar (DateTime comeco, DateTime fim)
        {
            string script = @"SELECT * FROM vw_fluxocaixa
                                      WHERE dt_operacao >= @comeco
                                        AND dt_operacao <= @fim
                                   ORDER BY dt_operacao,
                                            tp_operacao";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("comeco", comeco));
            parm.Add(new MySqlParameter("fim", fim));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<VIEW_FinanceiroDTO> fora = new List<VIEW_FinanceiroDTO>();

            while (reader.Read())
            {
                VIEW_FinanceiroDTO dentro = new VIEW_FinanceiroDTO();
                dentro.Data = reader.GetDateTime("dt_operacao");
                dentro.Total = reader.GetDecimal("vl_total");
                dentro.Operacao = reader.GetString("tp_operacao");

                fora.Add(dentro);
            }
            reader.Close();
            return fora;
        }
    }
}
