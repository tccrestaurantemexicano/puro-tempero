﻿using MySql.Data.MySqlClient;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Base;
using Nsf.PuroTempero.MODELO.Módulo_Venda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.PuroTempero.DATABASE.Módulo_Venda
{
    public class Database_VendaItem
    {
        public int Salvar (DTO_VendaItem dto)
        {
            string script =
            @"INSERT INTO tb_venda_item
            (
              id_venda,
              id_produto_venda
            )
            VALUES
            (
              @id_venda,
              @id_produto_venda
            )";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_venda", dto.ID_Venda));
            parm.Add(new MySqlParameter("id_produto_venda", dto.ID_Produto));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parm);
        }
    }
}
