﻿using MySql.Data.MySqlClient;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Venda.Vendas
{
    public class Database_Vendas
    {
        public int Salvar(DTO_Vendas dto)
        {
            string script = 
            @"INSERT INTO tb_venda
            (
                id_cliente, 
                dt_venda
            )
            VALUES
            (
                @id_cliente, 
                @dt_venda
            )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", dto.ID_Cliente));
            parms.Add(new MySqlParameter("dt_venda", dto.Data));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }

        public void Alterar(DTO_Vendas dto)
        {
            string script = @"UPDATE tb_venda 
                                 SET id_cliente      =@id_cliente,
                                     dt_venda        =@dt_venda
                               WHERE id_venda        = @id_venda";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_venda", dto.ID));
            parms.Add(new MySqlParameter("id_cliente", dto.ID_Cliente));
            parms.Add(new MySqlParameter("dt_venda", dto.Data));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(DTO_Vendas dto)
        {
            string script = @"DELETE FROM tb_venda 
                                    WHERE id_venda = @id_venda";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_venda", dto.ID));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<DTO_Vendas> Listar()
        {
            string script = @"SELECT * FROM tb_venda";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Vendas> Lista = new List<DTO_Vendas>();

            while (reader.Read())
            {
                DTO_Vendas dto = new DTO_Vendas();
                dto.ID = reader.GetInt32("id_venda");
                dto.ID_Cliente = reader.GetInt32("id_cliente");
                dto.Data = reader.GetDateTime("dt_venda");

                Lista.Add(dto);
            }
            reader.Close();
            return Lista;
        }

        public List<DTO_Vendas> Consultar(DateTime comeco, DateTime fim)
        {
            string script = @"SELECT * FROM tb_venda 
                                      WHERE dt_venda >= @comeco
                                        AND dt_venda <= @fim";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("comeco", comeco));
            parms.Add(new MySqlParameter("fim", fim));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Vendas> Lista = new List<DTO_Vendas>();

            while (reader.Read())
            {
                DTO_Vendas dto = new DTO_Vendas();
                dto.ID = reader.GetInt32("id_venda");
                dto.ID_Cliente = reader.GetInt32("id_cliente");
                dto.Data = reader.GetDateTime("dt_venda");

                Lista.Add(dto);
            }
            reader.Close();
            return Lista;
        }

    }
}
