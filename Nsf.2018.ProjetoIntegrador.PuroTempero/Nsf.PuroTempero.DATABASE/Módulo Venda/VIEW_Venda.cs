﻿using MySql.Data.MySqlClient;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Base;
using Nsf.PuroTempero.MODELO.Módulo_Venda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.PuroTempero.DATABASE.Módulo_Venda
{
    public class VIEW_Venda
    {
        public List<VIEW_VendaDTO> Consultar(DateTime comeco, DateTime fim)
        {
            string script = @"SELECT * FROM view_consultar_pedido_venda
                                      WHERE dt_venda >= @comeco
                                        AND dt_venda <= @fim";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("comeco", comeco));
            parm.Add(new MySqlParameter("fim", fim));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<VIEW_VendaDTO> fora = new List<VIEW_VendaDTO>();

            while (reader.Read())
            {
                VIEW_VendaDTO dentro = new VIEW_VendaDTO();
                dentro.ID = reader.GetInt32("id_venda");
                dentro.Data = reader.GetDateTime("dt_venda");
                dentro.Cliente = reader.GetString("nm_cliente");
                dentro.Quantidade = reader.GetInt32("qtd_itens");
                dentro.Total = reader.GetDecimal("vl_total");

                fora.Add(dentro);
            }
            reader.Close();
            return fora;
        }
    }
}
