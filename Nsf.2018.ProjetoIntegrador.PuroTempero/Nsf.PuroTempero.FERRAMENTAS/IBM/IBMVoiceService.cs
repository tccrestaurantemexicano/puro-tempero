﻿using Newtonsoft.Json;
using Stannieman.AudioPlayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.PuroTempero.FERRAMENTAS.IBM
{
    public class IBMVoiceService
    {
        //Para conseguir ter acesso a API de voz da IBM, é necessário o plugin NuGET Stannieman.AudioPlayer e NAudio. Além do Json.

        //Objeto de escopo player
        AudioPlayer player;

        public void Falar(string mensagem)
        {
            //Credênciais da IBM
            string url = "https://stream.watsonplatform.net/text-to-speech/api/v1/synthesize?voice=pt-BR_IsabelaVoice";
            string usuario = "b869da1d-e167-42ab-a92c-38fd95048f85";
            string senha = "a88tRoCXbs0p";

            //Cria um cliente para conectar na API da IBM
            HttpClient cliente = new HttpClient();      //Criação do objeto WEB
            cliente.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes(usuario + ":" + senha)));
            cliente.DefaultRequestHeaders.Accept.Add(MediaTypeWithQualityHeaderValue.Parse("audio/wav"));

            //Cria todo o conteúdo que será solicitado
            StringContent conteudo = new StringContent("{\"text\": \"" + mensagem + "\"}", Encoding.UTF8, "application/json");

            //Executa chamada POST para a API da IBM
            var respostaAPI = cliente.PostAsync(url, conteudo).Result;
            ExecutarAudio(respostaAPI);
        }
        private void ExecutarAudio(HttpResponseMessage respostaAPI)
        {
            //Converter resposta em Bytes
            Stream ibmvoz = respostaAPI.Content.ReadAsStreamAsync().Result;
            string arquivo = "voz.wav";     //Pegando a extensão do áudio

            //Se o arquivo do áudio existir, deleta
            if (File.Exists(arquivo))
                File.Delete(arquivo);

            //Salvar arquivo de áudio
            using (FileStream fs = new FileStream(arquivo, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                ibmvoz.CopyTo(fs);
            }

            if (player == null)
                player = new AudioPlayer();

            //Executar o áudio
            player.SetFileAsync(arquivo, arquivo).RunSynchronously();
            player.PlayAsync().RunSynchronously();
        }
        public void IniciarOuvir()
        {
            //Para player de áudio
            if (player != null)
            {
                player.Dispose();
                player = null;
            }
            //Inicia a gravação do áudio pelo microfone
            mciSendString("Open new Type waveaudio alias meuaudio", null, 0, IntPtr.Zero);
            mciSendString("record meuaudio", null, 0, IntPtr.Zero);
        }
        public string PararOuvir()
        {
            //Apagar arquivo de áudio existente
            string arquivo = "meusom.wav";
            //if (File.Exists(arquivo))
            //    File.Delete(arquivo);

            //Salva no arquivo de áudio
            mciSendString("save meuaudio " + arquivo, null, 0, IntPtr.Zero);
            mciSendString("close meuaudio", null, 0, IntPtr.Zero);

            //Configura credenciais da API do IBM
            string url = "https://stream.watsonplatform.net/speech-to-text/api/v1/recognize?model=pt-BR_NarrowbandModel";
            string usuario = "c4e43837-e1d6-42df-9788-7af41e14b949";
            string senha = "na2ABajmkmGp";

            //Cria um cliente para Conectar na API da IBM
            HttpClient cliente = new HttpClient();
            cliente.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",Convert.ToBase64String(Encoding.ASCII.GetBytes(usuario + ":" + senha)));
            cliente.DefaultRequestHeaders.Accept.Add(MediaTypeWithQualityHeaderValue.Parse("application/json"));

            //Cria conteúdo do cliente
            StreamContent content = new StreamContent(new FileStream(arquivo, FileMode.Open, FileAccess.Read));

            //Executa chamada POST na API (Resposta em JSON)
            var ibmtexto = cliente.PostAsync(url, content).Result.Content.ReadAsStringAsync().Result;

            //Converte JSON para classe
            IBMVoiceResult obj = JsonConvert.DeserializeObject<IBMVoiceResult>(ibmtexto);

            //Retornar o texto
            string texto = obj.Results[0].Alternatives[0].Transcript;
            return texto;

        }

        [DllImport("winmm.dll")]
        private static extern long mciSendString(string comando, StringBuilder sb, int lenght, IntPtr cb);
    }
}