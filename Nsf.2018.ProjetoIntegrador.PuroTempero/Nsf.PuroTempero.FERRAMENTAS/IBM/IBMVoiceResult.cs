﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.PuroTempero.FERRAMENTAS.IBM
{
    class IBMVoiceResult
    {
        public List<IBMResultItem> Results { get; set; }
        public int ResultIndex { get; set; }
    }

    public class IBMResultItem
    {
        public List<IbmMessage> Alternatives { get; set; }
        public bool Final { get; set; }
    }

    public class IbmMessage
    {
        public double Confidence { get; set; }
        public string Transcript { get; set; }
    }
}
