﻿using MessagingToolkit.QRCode.Codec;
using MessagingToolkit.QRCode.Codec.Data;
using Spire.Barcode;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.PuroTempero.FERRAMENTAS.QRCode
{
    public class QRCodeService
    {
        //Para conseguir fazer o controle por código de barra é necessário utilizar o plugin o NuGET MessagingToolkit.QRCode e Spire.Barcode
        public Image SalvarQRCode (string mensagem, string arquivo = null)
        {
            //Configuração do código de barras
            BarcodeSettings config = new BarcodeSettings();
            config.ShowText = false;            //Pelo o que eu percebi, indica se irá mostrar o código de barra como texto
            config.ShowTopText = false;         //Indica se irá mostrar a informação a frente de todos os outros controles
            config.ShowCheckSumChar = false;    //Indica se irá mostrar o código nos padrões Code18 e Ean128
            config.ShowTextOnBottom = false;    //Indica se irá mostrar o texto do código de barra no botão

            config.QRCodeDataMode = QRCodeDataMode.AlphaNumber;     //Pegando o valor em Código de barra em QRCode, e o transformando em dígitos
            config.Type = BarCodeType.QRCode;                       //O Type será QRCode, ao invés de, Barcode
            config.QRCodeECL = QRCodeECL.H;                         //Pelo o que eu entendi, é um método que previni erros
            config.X = 1.0f;                                        //Largura do QRCode ou código de barra.

            //Atribui valor ao QRCode
            config.Data = mensagem;         //O valor para renderizar o QRCode
            config.Data2D = mensagem;       //Seta o texto em 2D

            //Gera o QRCode
            BarCodeGenerator qrcode = new BarCodeGenerator(config);
            Image code = qrcode.GenerateImage();

            //Salva a Imagem
            if (!string.IsNullOrEmpty(arquivo))
                code.Save(arquivo.Replace(".png", "") + ".png", ImageFormat.Png);

            //Retorna a imagem com o valor em QRCode
            return code;
        }

        public string LerQRCode (Image arquivo)
        {
            //Cria objeto para ler o QRCode
            QRCodeDecoder qrcode = new QRCodeDecoder();

            //Lê o valor do QRCode
            string dado = qrcode.Decode(new QRCodeBitmapImage(arquivo as Bitmap));  //Com o objeto em "mãos", É decodificado a imagem em bytes/pixel.
            return dado;
        }
        public string LerQRCode (string arquivo)
        {
            //Cria objeto para ler o QRCode
            QRCodeDecoder qrcode = new QRCodeDecoder();

            //Carrega a imagem do QRCode
            Bitmap imagem = Bitmap.FromStream(LerImagem(arquivo)) as Bitmap;

            //Lê o valor do QRCode
            string dado = qrcode.Decode(new QRCodeBitmapImage(imagem));     //Com o objeto em "mãos", É decodificado a imagem em bytes/pixel.
            return dado;
        }
        private MemoryStream LerImagem (string arquivo)
        {
            //Cria um objeto com espaçamento de mémoria gerado em bytes, utilizado como "Backup"
            MemoryStream ms = new MemoryStream();

            //FileMode = Especifica o modo como deve se aberto
            //FileAcess = Define maneiras para conseguir ter acesso a leitura de um determinado dado
            //FileStream = É uma pacote com uma sequência de bytes onde se pode fazer leitura e gravações, além de suportes
            using (FileStream fs = new FileStream(arquivo, FileMode.Open, FileAccess.Read))
            {
                fs.CopyTo(ms);   //Lê os bytes presentes no fluxo atual, e depois cola em um outro fluxo de "backup"
            }
            return ms;
        }
    }
}
