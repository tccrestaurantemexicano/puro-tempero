﻿using Nsf.PuroTempero.DATABASE.Módulo_Venda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Venda.Produtos
{
    public class Business_ProdutoVenda
    {
        Database_ProdutoVenda db = new Database_ProdutoVenda();

        public int Salvar(DTO_ProdutoVenda dto)
        {
            return db.Salvar(dto);
        }
        public void Remover(int dto)
        {
            db.Remover(dto);
        }
        public void Alterar(DTO_ProdutoVenda dto)
        {
            db.Alterar(dto);
        }
        public List<DTO_ProdutoVenda> Listar()
        {
            List<DTO_ProdutoVenda> list = db.Listar();
            return list;
        }
        public List<DTO_ProdutoVenda> Consultar(DTO_ProdutoVenda dto)
        {
            List<DTO_ProdutoVenda> consult =  db.Consultar(dto);
            return consult;
        }

    }
}
