﻿using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Venda.Produtos;
using Nsf.PuroTempero.BUSINESS.Módulo_Venda;
using Nsf.PuroTempero.MODELO.Módulo_Venda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Venda.Vendas
{
    public class Business_Vendas
    {
        Database_Vendas db = new Database_Vendas();

        public int Salvar(DTO_Vendas vendas, List<DTO_ProdutoVenda> produtos)
        {
            //Salvo o ID
            int idVenda = db.Salvar(vendas);

            //Chamo a business que contém os IDS de VENDA e PRODUTO
            Business_VendaItem bussines = new Business_VendaItem();

            //Para cada produto encontrado
            foreach (DTO_ProdutoVenda item in produtos)
            {
                //INSTANCIO a tabela filha
                DTO_VendaItem dto = new DTO_VendaItem();

                //Passo o ID do produto encontrado no paramêtro do método
                dto.ID_Produto = item.ID;

                //Chamo a variável que contém o ID da venda
                dto.ID_Venda = idVenda;

                //Chamo o método de salvar da tabela filha, pois assim, eu irei conseguir trabalhar com as FKs
                bussines.Salvar(dto);
            }
            //Retorno o valor encontrado no ID da venda
            return idVenda;
            
        }
        public void Alterar(DTO_Vendas dto)
        {
            db.Alterar(dto);
        }
        public void Remover(DTO_Vendas dto)
        {
            db.Remover(dto);
        }
        public List<DTO_Vendas> Listar()
        {
            List<DTO_Vendas> list =  db.Listar();
            return list;
        }
        public List<DTO_Vendas> ConsultarCliente(DateTime comeco, DateTime fim)
        {
            List<DTO_Vendas> consult = db.Consultar(comeco, fim);
            return consult;
        }
    }
}
