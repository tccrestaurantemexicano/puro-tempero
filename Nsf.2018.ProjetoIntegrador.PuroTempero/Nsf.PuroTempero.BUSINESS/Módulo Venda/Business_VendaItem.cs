﻿using Nsf.PuroTempero.DATABASE.Módulo_Venda;
using Nsf.PuroTempero.MODELO.Módulo_Venda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.PuroTempero.BUSINESS.Módulo_Venda
{
    public class Business_VendaItem
    {
        public int Salvar (DTO_VendaItem dto)
        {
            Database_VendaItem db = new Database_VendaItem();
            return db.Salvar(dto);
        }
    }
}
