﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Acesso.Funcionário
{
    public class DTO_Funcionario
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string RG { get; set; }
        public string CPF { get; set; }
        public DateTime Nascimento { get; set; }
        public string Sexo { get; set; }
        public decimal Transporte { get; set; }
        public decimal Refeicao { get; set; }
        public decimal Alimentacao { get; set; }
        public decimal Convenio { get; set; }
        public decimal Salario { get; set; }
        public string Foto { get; set; }
        public string Usuario { get; set; }
        public string Senha { get; set; }
        public string UF { get; set; }
        public string Cidade { get; set; }
        public string Endereço { get; set; }
        public int Numero { get; set; }
        public string CEP { get; set; }
        public string Complemento { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
        public string Celular { get; set; }
    }
}
