﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.PuroTempero.MODELO.Módulo_Venda
{
    public class DTO_VendaItem
    {
        public int ID { get; set; }
        public int ID_Venda { get; set; }
        public int ID_Produto { get; set; }
    }
}
