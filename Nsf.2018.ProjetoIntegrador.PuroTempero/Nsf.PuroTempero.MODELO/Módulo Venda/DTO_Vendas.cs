﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Venda.Vendas
{
    public class DTO_Vendas
    {
        public int ID { get; set; }
        public int ID_Cliente { get; set; }
        public DateTime Data { get; set; }
    }
}
