﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_RH
{
    public class DTO_RH
    {
        public int id_ponto{ get; set; }
        public int id_funcionario{ get; set; }
        public DateTime Data{ get; set; }
        public string hr_entrada{ get; set; }
        public string hr_almoco_saida { get; set; }
        public string hr_almoco_retorno { get; set; }
        public string hr_saida{ get; set; }
    }
}
