﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras
{
    public class DTO_Compra
    {
        public int ID { get; set; }
        public int ID_Fornecedor { get; set; }
        public DateTime Data_Compra { get; set; }   
    }
}
