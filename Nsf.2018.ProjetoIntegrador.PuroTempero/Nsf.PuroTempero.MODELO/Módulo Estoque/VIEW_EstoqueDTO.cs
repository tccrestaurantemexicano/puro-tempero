﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.PuroTempero.MODELO.Módulo_Estoque
{
    public class VIEW_EstoqueDTO
    {
        public int ID { get; set; }
        public int ID_Produto { get; set; }
        public string Fornecedor { get; set; }
        public string Produto { get; set; }
        public string Marca { get; set; }
        public decimal Preco { get; set; }
        public int Quantidade { get; set; }
        public decimal Total { get; set; }
    }
}
