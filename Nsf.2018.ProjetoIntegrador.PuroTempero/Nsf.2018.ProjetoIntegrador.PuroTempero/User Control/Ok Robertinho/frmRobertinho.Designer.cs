﻿namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Ok_Jarvis
{
    partial class frmRobertinho
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRobertinho));
            this.imgMicrofone = new System.Windows.Forms.PictureBox();
            this.txtTexto = new System.Windows.Forms.TextBox();
            this.imgGif = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgMicrofone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgGif)).BeginInit();
            this.SuspendLayout();
            // 
            // imgMicrofone
            // 
            this.imgMicrofone.Image = global::Nsf._2018.ProjetoIntegrador.PuroTempero.Properties.Resources.icons8_microfone_641;
            this.imgMicrofone.Location = new System.Drawing.Point(286, 3);
            this.imgMicrofone.Name = "imgMicrofone";
            this.imgMicrofone.Size = new System.Drawing.Size(30, 41);
            this.imgMicrofone.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgMicrofone.TabIndex = 6;
            this.imgMicrofone.TabStop = false;
            this.imgMicrofone.Click += new System.EventHandler(this.imgMicrofone_Click);
            this.imgMicrofone.DoubleClick += new System.EventHandler(this.imgMicrofone_DoubleClick);
            // 
            // txtTexto
            // 
            this.txtTexto.BackColor = System.Drawing.SystemColors.MenuBar;
            this.txtTexto.Location = new System.Drawing.Point(6, 13);
            this.txtTexto.Name = "txtTexto";
            this.txtTexto.Size = new System.Drawing.Size(279, 27);
            this.txtTexto.TabIndex = 5;
            // 
            // imgGif
            // 
            this.imgGif.Image = global::Nsf._2018.ProjetoIntegrador.PuroTempero.Properties.Resources.google_voice_effect;
            this.imgGif.Location = new System.Drawing.Point(6, 50);
            this.imgGif.Name = "imgGif";
            this.imgGif.Size = new System.Drawing.Size(310, 152);
            this.imgGif.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgGif.TabIndex = 4;
            this.imgGif.TabStop = false;
            // 
            // frmRobertinho
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(323, 209);
            this.Controls.Add(this.imgMicrofone);
            this.Controls.Add(this.txtTexto);
            this.Controls.Add(this.imgGif);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmRobertinho";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Puro Tempero | Robertinho";
            ((System.ComponentModel.ISupportInitialize)(this.imgMicrofone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgGif)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox imgMicrofone;
        private System.Windows.Forms.TextBox txtTexto;
        private System.Windows.Forms.PictureBox imgGif;
    }
}