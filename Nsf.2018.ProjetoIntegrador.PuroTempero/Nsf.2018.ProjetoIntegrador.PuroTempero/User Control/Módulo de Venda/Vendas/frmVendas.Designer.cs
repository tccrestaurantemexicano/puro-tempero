﻿namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Venda.Vendas
{
    partial class frmVendas
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlCentroDireito = new System.Windows.Forms.Panel();
            this.pnlBaixoConfig = new System.Windows.Forms.Panel();
            this.pnlCimaConfig = new System.Windows.Forms.Panel();
            this.btnDeletar = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnMenos = new System.Windows.Forms.Button();
            this.btnMais = new System.Windows.Forms.Button();
            this.pnlCentroCima = new System.Windows.Forms.Panel();
            this.lstVenda = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pnlCentroBaixo = new System.Windows.Forms.Panel();
            this.btnFinalizar = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.lblVerdinha = new System.Windows.Forms.Label();
            this.lblSifrao = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.pnlLadoDireito = new System.Windows.Forms.Panel();
            this.pnlLadoEsquerdo = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblVendaProdutos = new System.Windows.Forms.Label();
            this.txtCodBarra = new System.Windows.Forms.TextBox();
            this.btnAddCarrinho = new System.Windows.Forms.Button();
            this.lblCodBarra = new System.Windows.Forms.Label();
            this.lblID = new System.Windows.Forms.Label();
            this.txtID = new System.Windows.Forms.TextBox();
            this.lblProduto = new System.Windows.Forms.Label();
            this.txtProduto = new System.Windows.Forms.TextBox();
            this.lblFornecedor = new System.Windows.Forms.Label();
            this.txtFornecedor = new System.Windows.Forms.TextBox();
            this.lblPreco = new System.Windows.Forms.Label();
            this.txtPreco = new System.Windows.Forms.TextBox();
            this.lblDesconto = new System.Windows.Forms.Label();
            this.txtDesconto = new System.Windows.Forms.TextBox();
            this.pnlCentroDireito.SuspendLayout();
            this.pnlCentroCima.SuspendLayout();
            this.pnlCentroBaixo.SuspendLayout();
            this.pnlLadoEsquerdo.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlCentroDireito
            // 
            this.pnlCentroDireito.Controls.Add(this.pnlBaixoConfig);
            this.pnlCentroDireito.Controls.Add(this.pnlCimaConfig);
            this.pnlCentroDireito.Controls.Add(this.btnDeletar);
            this.pnlCentroDireito.Controls.Add(this.btnAdd);
            this.pnlCentroDireito.Controls.Add(this.btnMenos);
            this.pnlCentroDireito.Controls.Add(this.btnMais);
            this.pnlCentroDireito.Controls.Add(this.pnlCentroCima);
            this.pnlCentroDireito.Controls.Add(this.pnlCentroBaixo);
            this.pnlCentroDireito.Controls.Add(this.pnlLadoDireito);
            this.pnlCentroDireito.Controls.Add(this.pnlLadoEsquerdo);
            this.pnlCentroDireito.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlCentroDireito.Location = new System.Drawing.Point(636, 0);
            this.pnlCentroDireito.Margin = new System.Windows.Forms.Padding(5);
            this.pnlCentroDireito.Name = "pnlCentroDireito";
            this.pnlCentroDireito.Size = new System.Drawing.Size(257, 599);
            this.pnlCentroDireito.TabIndex = 0;
            // 
            // pnlBaixoConfig
            // 
            this.pnlBaixoConfig.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.pnlBaixoConfig.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBaixoConfig.Location = new System.Drawing.Point(10, 421);
            this.pnlBaixoConfig.Name = "pnlBaixoConfig";
            this.pnlBaixoConfig.Size = new System.Drawing.Size(237, 20);
            this.pnlBaixoConfig.TabIndex = 3;
            // 
            // pnlCimaConfig
            // 
            this.pnlCimaConfig.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.pnlCimaConfig.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlCimaConfig.Location = new System.Drawing.Point(10, 359);
            this.pnlCimaConfig.Name = "pnlCimaConfig";
            this.pnlCimaConfig.Size = new System.Drawing.Size(237, 18);
            this.pnlCimaConfig.TabIndex = 3;
            // 
            // btnDeletar
            // 
            this.btnDeletar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(64)))), ((int)(((byte)(55)))));
            this.btnDeletar.FlatAppearance.BorderSize = 0;
            this.btnDeletar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeletar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnDeletar.Location = new System.Drawing.Point(76, 383);
            this.btnDeletar.Name = "btnDeletar";
            this.btnDeletar.Size = new System.Drawing.Size(105, 32);
            this.btnDeletar.TabIndex = 5;
            this.btnDeletar.Text = "Deletar";
            this.btnDeletar.UseVisualStyleBackColor = false;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(150)))), ((int)(((byte)(136)))));
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAdd.Location = new System.Drawing.Point(184, 383);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(58, 32);
            this.btnAdd.TabIndex = 5;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            // 
            // btnMenos
            // 
            this.btnMenos.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnMenos.FlatAppearance.BorderSize = 0;
            this.btnMenos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMenos.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnMenos.Location = new System.Drawing.Point(45, 383);
            this.btnMenos.Name = "btnMenos";
            this.btnMenos.Size = new System.Drawing.Size(28, 32);
            this.btnMenos.TabIndex = 5;
            this.btnMenos.Text = "-";
            this.btnMenos.UseVisualStyleBackColor = false;
            // 
            // btnMais
            // 
            this.btnMais.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnMais.FlatAppearance.BorderSize = 0;
            this.btnMais.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMais.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnMais.Location = new System.Drawing.Point(13, 383);
            this.btnMais.Name = "btnMais";
            this.btnMais.Size = new System.Drawing.Size(29, 32);
            this.btnMais.TabIndex = 5;
            this.btnMais.Text = "+";
            this.btnMais.UseVisualStyleBackColor = false;
            // 
            // pnlCentroCima
            // 
            this.pnlCentroCima.Controls.Add(this.lstVenda);
            this.pnlCentroCima.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlCentroCima.Location = new System.Drawing.Point(10, 0);
            this.pnlCentroCima.Name = "pnlCentroCima";
            this.pnlCentroCima.Size = new System.Drawing.Size(237, 359);
            this.pnlCentroCima.TabIndex = 3;
            // 
            // lstVenda
            // 
            this.lstVenda.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstVenda.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lstVenda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstVenda.GridLines = true;
            this.lstVenda.Location = new System.Drawing.Point(0, 0);
            this.lstVenda.Name = "lstVenda";
            this.lstVenda.Size = new System.Drawing.Size(237, 359);
            this.lstVenda.TabIndex = 0;
            this.lstVenda.UseCompatibleStateImageBehavior = false;
            this.lstVenda.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Produtos";
            this.columnHeader1.Width = 90;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Qtd";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Preço";
            this.columnHeader3.Width = 80;
            // 
            // pnlCentroBaixo
            // 
            this.pnlCentroBaixo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.pnlCentroBaixo.Controls.Add(this.btnFinalizar);
            this.pnlCentroBaixo.Controls.Add(this.btnLimpar);
            this.pnlCentroBaixo.Controls.Add(this.lblVerdinha);
            this.pnlCentroBaixo.Controls.Add(this.lblSifrao);
            this.pnlCentroBaixo.Controls.Add(this.lblTotal);
            this.pnlCentroBaixo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlCentroBaixo.Location = new System.Drawing.Point(10, 441);
            this.pnlCentroBaixo.Name = "pnlCentroBaixo";
            this.pnlCentroBaixo.Size = new System.Drawing.Size(237, 158);
            this.pnlCentroBaixo.TabIndex = 3;
            // 
            // btnFinalizar
            // 
            this.btnFinalizar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(175)))), ((int)(((byte)(80)))));
            this.btnFinalizar.FlatAppearance.BorderSize = 0;
            this.btnFinalizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFinalizar.ForeColor = System.Drawing.Color.White;
            this.btnFinalizar.Location = new System.Drawing.Point(120, 117);
            this.btnFinalizar.Name = "btnFinalizar";
            this.btnFinalizar.Size = new System.Drawing.Size(112, 32);
            this.btnFinalizar.TabIndex = 5;
            this.btnFinalizar.Text = "Finalizar";
            this.btnFinalizar.UseVisualStyleBackColor = false;
            // 
            // btnLimpar
            // 
            this.btnLimpar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
            this.btnLimpar.FlatAppearance.BorderSize = 0;
            this.btnLimpar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLimpar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnLimpar.Location = new System.Drawing.Point(5, 117);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(112, 32);
            this.btnLimpar.TabIndex = 5;
            this.btnLimpar.Text = "Limpar";
            this.btnLimpar.UseVisualStyleBackColor = false;
            // 
            // lblVerdinha
            // 
            this.lblVerdinha.AutoSize = true;
            this.lblVerdinha.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVerdinha.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.lblVerdinha.Location = new System.Drawing.Point(90, 62);
            this.lblVerdinha.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblVerdinha.Name = "lblVerdinha";
            this.lblVerdinha.Size = new System.Drawing.Size(81, 23);
            this.lblVerdinha.TabIndex = 1;
            this.lblVerdinha.Text = "1000,00";
            this.lblVerdinha.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblSifrao
            // 
            this.lblSifrao.AutoSize = true;
            this.lblSifrao.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSifrao.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.lblSifrao.Location = new System.Drawing.Point(59, 61);
            this.lblSifrao.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblSifrao.Name = "lblSifrao";
            this.lblSifrao.Size = new System.Drawing.Size(36, 23);
            this.lblSifrao.TabIndex = 1;
            this.lblSifrao.Text = "RS:";
            this.lblSifrao.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.lblTotal.Location = new System.Drawing.Point(24, 30);
            this.lblTotal.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(59, 23);
            this.lblTotal.TabIndex = 1;
            this.lblTotal.Text = "Total:";
            this.lblTotal.Click += new System.EventHandler(this.label1_Click);
            // 
            // pnlLadoDireito
            // 
            this.pnlLadoDireito.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.pnlLadoDireito.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlLadoDireito.Location = new System.Drawing.Point(247, 0);
            this.pnlLadoDireito.Name = "pnlLadoDireito";
            this.pnlLadoDireito.Size = new System.Drawing.Size(10, 599);
            this.pnlLadoDireito.TabIndex = 4;
            // 
            // pnlLadoEsquerdo
            // 
            this.pnlLadoEsquerdo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.pnlLadoEsquerdo.Controls.Add(this.panel5);
            this.pnlLadoEsquerdo.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLadoEsquerdo.Location = new System.Drawing.Point(0, 0);
            this.pnlLadoEsquerdo.Name = "pnlLadoEsquerdo";
            this.pnlLadoEsquerdo.Size = new System.Drawing.Size(10, 599);
            this.pnlLadoEsquerdo.TabIndex = 3;
            // 
            // panel5
            // 
            this.panel5.Location = new System.Drawing.Point(10, 301);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(162, 16);
            this.panel5.TabIndex = 3;
            // 
            // lblVendaProdutos
            // 
            this.lblVendaProdutos.AutoSize = true;
            this.lblVendaProdutos.Font = new System.Drawing.Font("Century Gothic", 16.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVendaProdutos.ForeColor = System.Drawing.Color.ForestGreen;
            this.lblVendaProdutos.Location = new System.Drawing.Point(228, 86);
            this.lblVendaProdutos.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblVendaProdutos.Name = "lblVendaProdutos";
            this.lblVendaProdutos.Size = new System.Drawing.Size(227, 26);
            this.lblVendaProdutos.TabIndex = 1;
            this.lblVendaProdutos.Text = "Venda dos Produtos";
            this.lblVendaProdutos.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtCodBarra
            // 
            this.txtCodBarra.Enabled = false;
            this.txtCodBarra.Location = new System.Drawing.Point(416, 188);
            this.txtCodBarra.Margin = new System.Windows.Forms.Padding(5);
            this.txtCodBarra.MaxLength = 20;
            this.txtCodBarra.Name = "txtCodBarra";
            this.txtCodBarra.Size = new System.Drawing.Size(198, 27);
            this.txtCodBarra.TabIndex = 1;
            // 
            // btnAddCarrinho
            // 
            this.btnAddCarrinho.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.btnAddCarrinho.FlatAppearance.BorderSize = 0;
            this.btnAddCarrinho.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddCarrinho.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddCarrinho.Location = new System.Drawing.Point(462, 364);
            this.btnAddCarrinho.Name = "btnAddCarrinho";
            this.btnAddCarrinho.Size = new System.Drawing.Size(152, 32);
            this.btnAddCarrinho.TabIndex = 6;
            this.btnAddCarrinho.Text = "Add ao Carrinho";
            this.btnAddCarrinho.UseVisualStyleBackColor = false;
            // 
            // lblCodBarra
            // 
            this.lblCodBarra.AutoSize = true;
            this.lblCodBarra.Font = new System.Drawing.Font("Century Gothic", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodBarra.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.lblCodBarra.Location = new System.Drawing.Point(318, 192);
            this.lblCodBarra.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblCodBarra.Name = "lblCodBarra";
            this.lblCodBarra.Size = new System.Drawing.Size(101, 19);
            this.lblCodBarra.TabIndex = 1;
            this.lblCodBarra.Text = "Cód. Barra:";
            this.lblCodBarra.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Font = new System.Drawing.Font("Century Gothic", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.lblID.Location = new System.Drawing.Point(89, 192);
            this.lblID.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(31, 19);
            this.lblID.TabIndex = 1;
            this.lblID.Text = "ID:";
            this.lblID.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtID
            // 
            this.txtID.Enabled = false;
            this.txtID.Location = new System.Drawing.Point(117, 188);
            this.txtID.Margin = new System.Windows.Forms.Padding(5);
            this.txtID.MaxLength = 8;
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(186, 27);
            this.txtID.TabIndex = 0;
            // 
            // lblProduto
            // 
            this.lblProduto.AutoSize = true;
            this.lblProduto.Font = new System.Drawing.Font("Century Gothic", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProduto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.lblProduto.Location = new System.Drawing.Point(44, 229);
            this.lblProduto.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblProduto.Name = "lblProduto";
            this.lblProduto.Size = new System.Drawing.Size(76, 19);
            this.lblProduto.TabIndex = 1;
            this.lblProduto.Text = "Produto:";
            this.lblProduto.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtProduto
            // 
            this.txtProduto.Enabled = false;
            this.txtProduto.Location = new System.Drawing.Point(117, 225);
            this.txtProduto.Margin = new System.Windows.Forms.Padding(5);
            this.txtProduto.MaxLength = 30;
            this.txtProduto.Name = "txtProduto";
            this.txtProduto.ReadOnly = true;
            this.txtProduto.Size = new System.Drawing.Size(186, 27);
            this.txtProduto.TabIndex = 2;
            // 
            // lblFornecedor
            // 
            this.lblFornecedor.AutoSize = true;
            this.lblFornecedor.Font = new System.Drawing.Font("Century Gothic", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFornecedor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.lblFornecedor.Location = new System.Drawing.Point(12, 266);
            this.lblFornecedor.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblFornecedor.Name = "lblFornecedor";
            this.lblFornecedor.Size = new System.Drawing.Size(108, 19);
            this.lblFornecedor.TabIndex = 1;
            this.lblFornecedor.Text = "Fornecedor:";
            this.lblFornecedor.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtFornecedor
            // 
            this.txtFornecedor.Location = new System.Drawing.Point(117, 262);
            this.txtFornecedor.Margin = new System.Windows.Forms.Padding(5);
            this.txtFornecedor.MaxLength = 50;
            this.txtFornecedor.Name = "txtFornecedor";
            this.txtFornecedor.ReadOnly = true;
            this.txtFornecedor.Size = new System.Drawing.Size(497, 27);
            this.txtFornecedor.TabIndex = 4;
            // 
            // lblPreco
            // 
            this.lblPreco.AutoSize = true;
            this.lblPreco.Font = new System.Drawing.Font("Century Gothic", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreco.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.lblPreco.Location = new System.Drawing.Point(356, 229);
            this.lblPreco.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblPreco.Name = "lblPreco";
            this.lblPreco.Size = new System.Drawing.Size(62, 19);
            this.lblPreco.TabIndex = 1;
            this.lblPreco.Text = "Preço:";
            this.lblPreco.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtPreco
            // 
            this.txtPreco.Enabled = false;
            this.txtPreco.Location = new System.Drawing.Point(416, 225);
            this.txtPreco.Margin = new System.Windows.Forms.Padding(5);
            this.txtPreco.MaxLength = 10;
            this.txtPreco.Name = "txtPreco";
            this.txtPreco.ReadOnly = true;
            this.txtPreco.Size = new System.Drawing.Size(198, 27);
            this.txtPreco.TabIndex = 3;
            // 
            // lblDesconto
            // 
            this.lblDesconto.AutoSize = true;
            this.lblDesconto.Font = new System.Drawing.Font("Century Gothic", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDesconto.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.lblDesconto.Location = new System.Drawing.Point(29, 303);
            this.lblDesconto.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.lblDesconto.Name = "lblDesconto";
            this.lblDesconto.Size = new System.Drawing.Size(92, 19);
            this.lblDesconto.TabIndex = 1;
            this.lblDesconto.Text = "Desconto:";
            this.lblDesconto.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtDesconto
            // 
            this.txtDesconto.Location = new System.Drawing.Point(117, 299);
            this.txtDesconto.Margin = new System.Windows.Forms.Padding(5);
            this.txtDesconto.MaxLength = 10;
            this.txtDesconto.Name = "txtDesconto";
            this.txtDesconto.Size = new System.Drawing.Size(198, 27);
            this.txtDesconto.TabIndex = 5;
            // 
            // frmVendas
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Controls.Add(this.btnAddCarrinho);
            this.Controls.Add(this.txtID);
            this.Controls.Add(this.txtFornecedor);
            this.Controls.Add(this.txtDesconto);
            this.Controls.Add(this.txtPreco);
            this.Controls.Add(this.txtProduto);
            this.Controls.Add(this.txtCodBarra);
            this.Controls.Add(this.lblID);
            this.Controls.Add(this.lblFornecedor);
            this.Controls.Add(this.lblDesconto);
            this.Controls.Add(this.lblPreco);
            this.Controls.Add(this.lblProduto);
            this.Controls.Add(this.lblCodBarra);
            this.Controls.Add(this.lblVendaProdutos);
            this.Controls.Add(this.pnlCentroDireito);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "frmVendas";
            this.Size = new System.Drawing.Size(893, 599);
            this.pnlCentroDireito.ResumeLayout(false);
            this.pnlCentroCima.ResumeLayout(false);
            this.pnlCentroBaixo.ResumeLayout(false);
            this.pnlCentroBaixo.PerformLayout();
            this.pnlLadoEsquerdo.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlCentroDireito;
        private System.Windows.Forms.Label lblVendaProdutos;
        private System.Windows.Forms.TextBox txtCodBarra;
        private System.Windows.Forms.Panel pnlLadoEsquerdo;
        private System.Windows.Forms.Panel pnlCentroCima;
        private System.Windows.Forms.Panel pnlCentroBaixo;
        private System.Windows.Forms.Panel pnlLadoDireito;
        private System.Windows.Forms.Button btnDeletar;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnMenos;
        private System.Windows.Forms.Button btnMais;
        private System.Windows.Forms.Button btnAddCarrinho;
        private System.Windows.Forms.Button btnFinalizar;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.Panel pnlBaixoConfig;
        private System.Windows.Forms.Panel pnlCimaConfig;
        private System.Windows.Forms.ListView lstVenda;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Label lblVerdinha;
        private System.Windows.Forms.Label lblSifrao;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lblCodBarra;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label lblProduto;
        private System.Windows.Forms.TextBox txtProduto;
        private System.Windows.Forms.Label lblFornecedor;
        private System.Windows.Forms.TextBox txtFornecedor;
        private System.Windows.Forms.Label lblPreco;
        private System.Windows.Forms.TextBox txtPreco;
        private System.Windows.Forms.Label lblDesconto;
        private System.Windows.Forms.TextBox txtDesconto;
    }
}
