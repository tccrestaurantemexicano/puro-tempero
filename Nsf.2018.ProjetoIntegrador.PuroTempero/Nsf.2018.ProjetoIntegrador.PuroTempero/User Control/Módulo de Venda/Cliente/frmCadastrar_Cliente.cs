﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Venda.Cliente;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Ferramentas.Imagem;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Ferramentas.Localização;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Cliente
{
    public partial class frmCadastrar_Cliente : UserControl
    {
        public frmCadastrar_Cliente()
        {
            InitializeComponent();
            CarregarCombo();

        }

        private void lblInfo_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.DarkGreen);
        }

        private void DrawGroupBox(GroupBox box, Graphics g, Color borderColor)
        {
            if (box != null)
            {
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);


                // Drawing Border
                //Left
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Right
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Bottom
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Top1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Top2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }
        }

        private void CarregarCombo ()
        {
            Localizacao uf = new Localizacao();
            cboUF.DataSource = uf.UF();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }
        private void SalvarCliente (DTO_Cliente dto)
        {
            dto.Nome = txtNome.Text;
            dto.CPF = txtCPF.Text;
            dto.RG = txtRG.Text;
            dto.Nascimento = dtpNascimento.Value;
            dto.Telefone = txtTelefone.Text;
            dto.Celular = txtCelular.Text;
            dto.Email = txtEmail.Text;
            dto.CEP = txtCEP.Text;
            dto.Endereco = txtEndereco.Text;
            dto.Complemento = txtComplemento.Text;
            dto.Casa = Convert.ToInt32(nudNumero.Value);
            dto.UF = cboUF.SelectedItem.ToString();
            dto.Cidade = txtCidade.Text;
            dto.PratoFavorito = rtxtPratosFavoritos.Text;
            dto.MesaFavorita = rtxtLocalFavorito.Text;
            dto.Frequencia = rtxtFrequencia.Text;
            dto.Imagem = ImagemPlugin.ConverterParaString(imgCliente.Image);

            VerificarGenero(dto);
        }
        private void VerificarGenero (DTO_Cliente genero)
        {
            if (rdnCasado.Checked == true)
            {
                 genero.Sexo = true;
            }
            else
            {
                genero.Sexo = false;
            }
        }
        private void btnSalvar_Click(object sender, EventArgs e)
        {
            DTO_Cliente dto = new DTO_Cliente();
            SalvarCliente(dto);

            Business_Cliente db = new Business_Cliente();
            db.Salvar(dto);
            MessageBox.Show("Cliente cadastrado com sucesso!",
                            "Puro Tempero!",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
        }

        private void btnProcurar_Click(object sender, EventArgs e)
        {
            //Instâncio a função que irá abrir os documentos Windows/Desktop.
            OpenFileDialog dialog = new OpenFileDialog();

            //Faz um filtro apenas em JPAG ou PNG
            dialog.Filter = "JPG Files(*.jpg)|*.jpg|PNG Files(*.png)|*.png";

            //Habilita a caixa de arquivos Windows.
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                //Pega a localização da imagem
                imgCliente.ImageLocation = dialog.FileName;
            }
        }
    }
}
