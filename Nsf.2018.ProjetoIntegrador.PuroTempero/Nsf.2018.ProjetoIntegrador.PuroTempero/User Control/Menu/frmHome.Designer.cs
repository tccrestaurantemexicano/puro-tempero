﻿namespace Nsf._2018.ProjetoIntegrador.PuroTempero.Forms.Login
{
    partial class frmHome
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmHome));
            this.lblVisaoGeral = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.imgVerdinhas = new System.Windows.Forms.PictureBox();
            this.lblVerdinhas = new System.Windows.Forms.Label();
            this.lblVenda = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.imgDespesas = new System.Windows.Forms.PictureBox();
            this.lblDespesas = new System.Windows.Forms.Label();
            this.lblCompras = new System.Windows.Forms.Label();
            this.pnlCliente = new System.Windows.Forms.Panel();
            this.imgClientes = new System.Windows.Forms.PictureBox();
            this.lblQuantidade = new System.Windows.Forms.Label();
            this.lblCliente = new System.Windows.Forms.Label();
            this.btnAtualizar = new System.Windows.Forms.Button();
            this.lblRelatorioSemanal = new System.Windows.Forms.Label();
            this.ProgressRecebidos = new Bunifu.Framework.UI.BunifuCircleProgressbar();
            this.ProgressPago = new Bunifu.Framework.UI.BunifuCircleProgressbar();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPago = new System.Windows.Forms.Label();
            this.lblPorcentagem = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgVerdinhas)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgDespesas)).BeginInit();
            this.pnlCliente.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgClientes)).BeginInit();
            this.SuspendLayout();
            // 
            // lblVisaoGeral
            // 
            this.lblVisaoGeral.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblVisaoGeral.AutoSize = true;
            this.lblVisaoGeral.Font = new System.Drawing.Font("Century Gothic", 18.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVisaoGeral.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(64)))), ((int)(((byte)(55)))));
            this.lblVisaoGeral.Location = new System.Drawing.Point(248, 15);
            this.lblVisaoGeral.Name = "lblVisaoGeral";
            this.lblVisaoGeral.Size = new System.Drawing.Size(434, 29);
            this.lblVisaoGeral.TabIndex = 0;
            this.lblVisaoGeral.Text = "Visão geral de Compras e Vendas";
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.panel1.Controls.Add(this.imgVerdinhas);
            this.panel1.Controls.Add(this.lblVerdinhas);
            this.panel1.Controls.Add(this.lblVenda);
            this.panel1.Location = new System.Drawing.Point(70, 66);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(222, 113);
            this.panel1.TabIndex = 1;
            // 
            // imgVerdinhas
            // 
            this.imgVerdinhas.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.imgVerdinhas.Image = ((System.Drawing.Image)(resources.GetObject("imgVerdinhas.Image")));
            this.imgVerdinhas.Location = new System.Drawing.Point(137, 16);
            this.imgVerdinhas.Name = "imgVerdinhas";
            this.imgVerdinhas.Size = new System.Drawing.Size(68, 80);
            this.imgVerdinhas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgVerdinhas.TabIndex = 1;
            this.imgVerdinhas.TabStop = false;
            // 
            // lblVerdinhas
            // 
            this.lblVerdinhas.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblVerdinhas.AutoSize = true;
            this.lblVerdinhas.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVerdinhas.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblVerdinhas.Location = new System.Drawing.Point(22, 59);
            this.lblVerdinhas.Name = "lblVerdinhas";
            this.lblVerdinhas.Size = new System.Drawing.Size(113, 23);
            this.lblVerdinhas.TabIndex = 0;
            this.lblVerdinhas.Text = "R$: 1000.00";
            // 
            // lblVenda
            // 
            this.lblVenda.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblVenda.AutoSize = true;
            this.lblVenda.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVenda.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblVenda.Location = new System.Drawing.Point(11, 16);
            this.lblVenda.Name = "lblVenda";
            this.lblVenda.Size = new System.Drawing.Size(72, 19);
            this.lblVenda.TabIndex = 0;
            this.lblVenda.Text = "Vendas:";
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
            this.panel2.Controls.Add(this.imgDespesas);
            this.panel2.Controls.Add(this.lblDespesas);
            this.panel2.Controls.Add(this.lblCompras);
            this.panel2.Location = new System.Drawing.Point(318, 66);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(222, 113);
            this.panel2.TabIndex = 1;
            // 
            // imgDespesas
            // 
            this.imgDespesas.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.imgDespesas.Image = ((System.Drawing.Image)(resources.GetObject("imgDespesas.Image")));
            this.imgDespesas.Location = new System.Drawing.Point(139, 16);
            this.imgDespesas.Name = "imgDespesas";
            this.imgDespesas.Size = new System.Drawing.Size(68, 80);
            this.imgDespesas.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgDespesas.TabIndex = 1;
            this.imgDespesas.TabStop = false;
            // 
            // lblDespesas
            // 
            this.lblDespesas.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblDespesas.AutoSize = true;
            this.lblDespesas.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDespesas.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblDespesas.Location = new System.Drawing.Point(24, 59);
            this.lblDespesas.Name = "lblDespesas";
            this.lblDespesas.Size = new System.Drawing.Size(113, 23);
            this.lblDespesas.TabIndex = 0;
            this.lblDespesas.Text = "R$: 1000.00";
            // 
            // lblCompras
            // 
            this.lblCompras.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCompras.AutoSize = true;
            this.lblCompras.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompras.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblCompras.Location = new System.Drawing.Point(13, 16);
            this.lblCompras.Name = "lblCompras";
            this.lblCompras.Size = new System.Drawing.Size(84, 19);
            this.lblCompras.TabIndex = 0;
            this.lblCompras.Text = "Compras:";
            // 
            // pnlCliente
            // 
            this.pnlCliente.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnlCliente.BackColor = System.Drawing.Color.RoyalBlue;
            this.pnlCliente.Controls.Add(this.imgClientes);
            this.pnlCliente.Controls.Add(this.lblQuantidade);
            this.pnlCliente.Controls.Add(this.lblCliente);
            this.pnlCliente.Location = new System.Drawing.Point(568, 66);
            this.pnlCliente.Name = "pnlCliente";
            this.pnlCliente.Size = new System.Drawing.Size(222, 113);
            this.pnlCliente.TabIndex = 1;
            // 
            // imgClientes
            // 
            this.imgClientes.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.imgClientes.Image = ((System.Drawing.Image)(resources.GetObject("imgClientes.Image")));
            this.imgClientes.Location = new System.Drawing.Point(131, 16);
            this.imgClientes.Name = "imgClientes";
            this.imgClientes.Size = new System.Drawing.Size(68, 80);
            this.imgClientes.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgClientes.TabIndex = 1;
            this.imgClientes.TabStop = false;
            // 
            // lblQuantidade
            // 
            this.lblQuantidade.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblQuantidade.AutoSize = true;
            this.lblQuantidade.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantidade.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblQuantidade.Location = new System.Drawing.Point(40, 59);
            this.lblQuantidade.Name = "lblQuantidade";
            this.lblQuantidade.Size = new System.Drawing.Size(43, 23);
            this.lblQuantidade.TabIndex = 0;
            this.lblQuantidade.Text = "215";
            // 
            // lblCliente
            // 
            this.lblCliente.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCliente.AutoSize = true;
            this.lblCliente.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblCliente.Location = new System.Drawing.Point(11, 16);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(67, 19);
            this.lblCliente.TabIndex = 0;
            this.lblCliente.Text = "Cliente:";
            // 
            // btnAtualizar
            // 
            this.btnAtualizar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAtualizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAtualizar.Image = ((System.Drawing.Image)(resources.GetObject("btnAtualizar.Image")));
            this.btnAtualizar.Location = new System.Drawing.Point(807, 8);
            this.btnAtualizar.Name = "btnAtualizar";
            this.btnAtualizar.Size = new System.Drawing.Size(40, 36);
            this.btnAtualizar.TabIndex = 3;
            this.btnAtualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAtualizar.UseVisualStyleBackColor = true;
            this.btnAtualizar.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblRelatorioSemanal
            // 
            this.lblRelatorioSemanal.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblRelatorioSemanal.AutoSize = true;
            this.lblRelatorioSemanal.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRelatorioSemanal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.lblRelatorioSemanal.Location = new System.Drawing.Point(66, 195);
            this.lblRelatorioSemanal.Name = "lblRelatorioSemanal";
            this.lblRelatorioSemanal.Size = new System.Drawing.Size(150, 19);
            this.lblRelatorioSemanal.TabIndex = 0;
            this.lblRelatorioSemanal.Text = "Relatório Semanal";
            // 
            // ProgressRecebidos
            // 
            this.ProgressRecebidos.animated = false;
            this.ProgressRecebidos.animationIterval = 5;
            this.ProgressRecebidos.animationSpeed = 300;
            this.ProgressRecebidos.BackColor = System.Drawing.Color.White;
            this.ProgressRecebidos.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ProgressRecebidos.BackgroundImage")));
            this.ProgressRecebidos.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F);
            this.ProgressRecebidos.ForeColor = System.Drawing.Color.SeaGreen;
            this.ProgressRecebidos.LabelVisible = false;
            this.ProgressRecebidos.LineProgressThickness = 8;
            this.ProgressRecebidos.LineThickness = 5;
            this.ProgressRecebidos.Location = new System.Drawing.Point(516, 322);
            this.ProgressRecebidos.Margin = new System.Windows.Forms.Padding(10, 9, 10, 9);
            this.ProgressRecebidos.MaxValue = 100;
            this.ProgressRecebidos.Name = "ProgressRecebidos";
            this.ProgressRecebidos.ProgressBackColor = System.Drawing.Color.Gainsboro;
            this.ProgressRecebidos.ProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.ProgressRecebidos.Size = new System.Drawing.Size(143, 143);
            this.ProgressRecebidos.TabIndex = 4;
            this.ProgressRecebidos.Value = 68;
            // 
            // ProgressPago
            // 
            this.ProgressPago.animated = false;
            this.ProgressPago.animationIterval = 5;
            this.ProgressPago.animationSpeed = 300;
            this.ProgressPago.BackColor = System.Drawing.Color.White;
            this.ProgressPago.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ProgressPago.BackgroundImage")));
            this.ProgressPago.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F);
            this.ProgressPago.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
            this.ProgressPago.LabelVisible = false;
            this.ProgressPago.LineProgressThickness = 8;
            this.ProgressPago.LineThickness = 5;
            this.ProgressPago.Location = new System.Drawing.Point(658, 322);
            this.ProgressPago.Margin = new System.Windows.Forms.Padding(10, 9, 10, 9);
            this.ProgressPago.MaxValue = 100;
            this.ProgressPago.Name = "ProgressPago";
            this.ProgressPago.ProgressBackColor = System.Drawing.Color.Gainsboro;
            this.ProgressPago.ProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
            this.ProgressPago.Size = new System.Drawing.Size(143, 143);
            this.ProgressPago.TabIndex = 4;
            this.ProgressPago.Value = 94;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.label1.Location = new System.Drawing.Point(544, 468);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Recebidos";
            // 
            // lblPago
            // 
            this.lblPago.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblPago.AutoSize = true;
            this.lblPago.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPago.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(67)))), ((int)(((byte)(54)))));
            this.lblPago.Location = new System.Drawing.Point(706, 468);
            this.lblPago.Name = "lblPago";
            this.lblPago.Size = new System.Drawing.Size(50, 19);
            this.lblPago.TabIndex = 0;
            this.lblPago.Text = "Pago";
            // 
            // lblPorcentagem
            // 
            this.lblPorcentagem.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblPorcentagem.AutoSize = true;
            this.lblPorcentagem.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPorcentagem.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblPorcentagem.Location = new System.Drawing.Point(537, 290);
            this.lblPorcentagem.Name = "lblPorcentagem";
            this.lblPorcentagem.Size = new System.Drawing.Size(241, 23);
            this.lblPorcentagem.TabIndex = 0;
            this.lblPorcentagem.Text = "Porcentagem de Capital";
            // 
            // frmHome
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Controls.Add(this.ProgressPago);
            this.Controls.Add(this.ProgressRecebidos);
            this.Controls.Add(this.btnAtualizar);
            this.Controls.Add(this.pnlCliente);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblVisaoGeral);
            this.Controls.Add(this.lblPago);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblPorcentagem);
            this.Controls.Add(this.lblRelatorioSemanal);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmHome";
            this.Size = new System.Drawing.Size(893, 555);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgVerdinhas)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgDespesas)).EndInit();
            this.pnlCliente.ResumeLayout(false);
            this.pnlCliente.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgClientes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblVisaoGeral;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox imgVerdinhas;
        private System.Windows.Forms.Label lblVerdinhas;
        private System.Windows.Forms.Label lblVenda;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox imgDespesas;
        private System.Windows.Forms.Label lblDespesas;
        private System.Windows.Forms.Label lblCompras;
        private System.Windows.Forms.Panel pnlCliente;
        private System.Windows.Forms.PictureBox imgClientes;
        private System.Windows.Forms.Label lblQuantidade;
        private System.Windows.Forms.Label lblCliente;
        private System.Windows.Forms.Button btnAtualizar;
        private System.Windows.Forms.Label lblRelatorioSemanal;
        private Bunifu.Framework.UI.BunifuCircleProgressbar ProgressRecebidos;
        private Bunifu.Framework.UI.BunifuCircleProgressbar ProgressPago;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPago;
        private System.Windows.Forms.Label lblPorcentagem;
    }
}
