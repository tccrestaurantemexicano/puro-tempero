﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Menu
{
    public partial class frmCompras : UserControl
    {
        public frmCompras()
        {
            InitializeComponent();
        }
        private void CarregarSubMenu(ContextMenuStrip contex, Control Button)
        {
            contex.Show(Button, 0, Button.Height);
        }

        private void btnPedido_Click(object sender, EventArgs e)
        {
            CarregarSubMenu(ContextPedido, btnPedido);
        }

        private void btnProduto_Click(object sender, EventArgs e)
        {
            CarregarSubMenu(ContextProduto, btnProduto);
        }
    }
}
