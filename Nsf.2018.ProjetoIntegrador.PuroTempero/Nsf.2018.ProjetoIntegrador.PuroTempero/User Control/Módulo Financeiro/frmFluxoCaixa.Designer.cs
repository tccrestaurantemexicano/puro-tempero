﻿namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_Financeiro
{
    partial class frmFluxoCaixa
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gpbBusca = new System.Windows.Forms.GroupBox();
            this.dtpFim = new System.Windows.Forms.DateTimePicker();
            this.dtpInicio = new System.Windows.Forms.DateTimePicker();
            this.dgvFinanceira = new System.Windows.Forms.DataGridView();
            this.btnConsultar = new System.Windows.Forms.Button();
            this.lblFim = new System.Windows.Forms.Label();
            this.lblInicio = new System.Windows.Forms.Label();
            this.gpbInformacoes = new System.Windows.Forms.GroupBox();
            this.imgImagem = new System.Windows.Forms.PictureBox();
            this.lblOBS = new System.Windows.Forms.Label();
            this.rtxtOBS = new System.Windows.Forms.RichTextBox();
            this.btnVoltar = new System.Windows.Forms.Button();
            this.data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.saida = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gpbBusca.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFinanceira)).BeginInit();
            this.gpbInformacoes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgImagem)).BeginInit();
            this.SuspendLayout();
            // 
            // gpbBusca
            // 
            this.gpbBusca.Controls.Add(this.dtpFim);
            this.gpbBusca.Controls.Add(this.dtpInicio);
            this.gpbBusca.Controls.Add(this.dgvFinanceira);
            this.gpbBusca.Controls.Add(this.btnConsultar);
            this.gpbBusca.Controls.Add(this.lblFim);
            this.gpbBusca.Controls.Add(this.lblInicio);
            this.gpbBusca.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbBusca.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbBusca.Location = new System.Drawing.Point(18, 3);
            this.gpbBusca.Name = "gpbBusca";
            this.gpbBusca.Size = new System.Drawing.Size(424, 397);
            this.gpbBusca.TabIndex = 28;
            this.gpbBusca.TabStop = false;
            this.gpbBusca.Text = "Busca ";
            this.gpbBusca.Paint += new System.Windows.Forms.PaintEventHandler(this.gpbBusca_Paint);
            // 
            // dtpFim
            // 
            this.dtpFim.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFim.Location = new System.Drawing.Point(76, 74);
            this.dtpFim.Name = "dtpFim";
            this.dtpFim.Size = new System.Drawing.Size(334, 22);
            this.dtpFim.TabIndex = 26;
            // 
            // dtpInicio
            // 
            this.dtpInicio.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpInicio.Location = new System.Drawing.Point(76, 37);
            this.dtpInicio.Name = "dtpInicio";
            this.dtpInicio.Size = new System.Drawing.Size(334, 22);
            this.dtpInicio.TabIndex = 25;
            // 
            // dgvFinanceira
            // 
            this.dgvFinanceira.AllowUserToAddRows = false;
            this.dgvFinanceira.AllowUserToDeleteRows = false;
            this.dgvFinanceira.AllowUserToResizeColumns = false;
            this.dgvFinanceira.AllowUserToResizeRows = false;
            this.dgvFinanceira.BackgroundColor = System.Drawing.Color.Snow;
            this.dgvFinanceira.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvFinanceira.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            this.dgvFinanceira.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvFinanceira.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFinanceira.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.data,
            this.saida,
            this.valor});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvFinanceira.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvFinanceira.GridColor = System.Drawing.Color.Snow;
            this.dgvFinanceira.Location = new System.Drawing.Point(16, 151);
            this.dgvFinanceira.MultiSelect = false;
            this.dgvFinanceira.Name = "dgvFinanceira";
            this.dgvFinanceira.ReadOnly = true;
            this.dgvFinanceira.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvFinanceira.RowHeadersVisible = false;
            this.dgvFinanceira.Size = new System.Drawing.Size(394, 232);
            this.dgvFinanceira.TabIndex = 24;
            this.dgvFinanceira.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvFinanceira_DataBindingComplete);
            // 
            // btnConsultar
            // 
            this.btnConsultar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConsultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultar.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultar.ForeColor = System.Drawing.Color.Black;
            this.btnConsultar.Location = new System.Drawing.Point(321, 107);
            this.btnConsultar.Name = "btnConsultar";
            this.btnConsultar.Size = new System.Drawing.Size(89, 27);
            this.btnConsultar.TabIndex = 3;
            this.btnConsultar.Text = "Consultar";
            this.btnConsultar.UseVisualStyleBackColor = true;
            this.btnConsultar.Click += new System.EventHandler(this.btnConsultar_Click);
            // 
            // lblFim
            // 
            this.lblFim.AutoSize = true;
            this.lblFim.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFim.ForeColor = System.Drawing.Color.Black;
            this.lblFim.Location = new System.Drawing.Point(13, 74);
            this.lblFim.Name = "lblFim";
            this.lblFim.Size = new System.Drawing.Size(42, 21);
            this.lblFim.TabIndex = 18;
            this.lblFim.Text = "Fim:";
            // 
            // lblInicio
            // 
            this.lblInicio.AutoSize = true;
            this.lblInicio.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInicio.ForeColor = System.Drawing.Color.Black;
            this.lblInicio.Location = new System.Drawing.Point(13, 37);
            this.lblInicio.Name = "lblInicio";
            this.lblInicio.Size = new System.Drawing.Size(57, 21);
            this.lblInicio.TabIndex = 17;
            this.lblInicio.Text = "Inicio:";
            // 
            // gpbInformacoes
            // 
            this.gpbInformacoes.Controls.Add(this.imgImagem);
            this.gpbInformacoes.Controls.Add(this.lblOBS);
            this.gpbInformacoes.Controls.Add(this.rtxtOBS);
            this.gpbInformacoes.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbInformacoes.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbInformacoes.Location = new System.Drawing.Point(447, 3);
            this.gpbInformacoes.Name = "gpbInformacoes";
            this.gpbInformacoes.Size = new System.Drawing.Size(142, 397);
            this.gpbInformacoes.TabIndex = 4;
            this.gpbInformacoes.TabStop = false;
            this.gpbInformacoes.Text = "Informações ";
            this.gpbInformacoes.Paint += new System.Windows.Forms.PaintEventHandler(this.gpbBusca_Paint);
            // 
            // imgImagem
            // 
            this.imgImagem.Image = global::Nsf._2018.ProjetoIntegrador.PuroTempero.Properties.Resources.Pesquisa1;
            this.imgImagem.Location = new System.Drawing.Point(10, 37);
            this.imgImagem.Name = "imgImagem";
            this.imgImagem.Size = new System.Drawing.Size(126, 179);
            this.imgImagem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgImagem.TabIndex = 29;
            this.imgImagem.TabStop = false;
            // 
            // lblOBS
            // 
            this.lblOBS.AutoSize = true;
            this.lblOBS.ForeColor = System.Drawing.Color.Black;
            this.lblOBS.Location = new System.Drawing.Point(6, 238);
            this.lblOBS.Name = "lblOBS";
            this.lblOBS.Size = new System.Drawing.Size(41, 20);
            this.lblOBS.TabIndex = 28;
            this.lblOBS.Text = "OBS:";
            // 
            // rtxtOBS
            // 
            this.rtxtOBS.Enabled = false;
            this.rtxtOBS.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtxtOBS.Location = new System.Drawing.Point(6, 261);
            this.rtxtOBS.Name = "rtxtOBS";
            this.rtxtOBS.Size = new System.Drawing.Size(130, 122);
            this.rtxtOBS.TabIndex = 27;
            this.rtxtOBS.Text = "Para fazer qualquer consulta é necessário ter privilégios definidos pelo Administ" +
    "rador.";
            // 
            // btnVoltar
            // 
            this.btnVoltar.BackColor = System.Drawing.Color.Maroon;
            this.btnVoltar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnVoltar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVoltar.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.btnVoltar.ForeColor = System.Drawing.Color.White;
            this.btnVoltar.Location = new System.Drawing.Point(506, 405);
            this.btnVoltar.Name = "btnVoltar";
            this.btnVoltar.Size = new System.Drawing.Size(83, 27);
            this.btnVoltar.TabIndex = 29;
            this.btnVoltar.Text = "Voltar";
            this.btnVoltar.UseVisualStyleBackColor = false;
            this.btnVoltar.Click += new System.EventHandler(this.btnVoltar_Click);
            // 
            // data
            // 
            this.data.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.data.DataPropertyName = "Data";
            this.data.HeaderText = "Data";
            this.data.Name = "data";
            this.data.ReadOnly = true;
            // 
            // saida
            // 
            this.saida.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.saida.DataPropertyName = "Operacao";
            this.saida.HeaderText = "Operacao";
            this.saida.Name = "saida";
            this.saida.ReadOnly = true;
            // 
            // valor
            // 
            this.valor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.valor.DataPropertyName = "Total";
            this.valor.HeaderText = "Valor";
            this.valor.Name = "valor";
            this.valor.ReadOnly = true;
            // 
            // frmFluxoCaixa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gpbBusca);
            this.Controls.Add(this.btnVoltar);
            this.Controls.Add(this.gpbInformacoes);
            this.Name = "frmFluxoCaixa";
            this.Size = new System.Drawing.Size(607, 435);
            this.gpbBusca.ResumeLayout(false);
            this.gpbBusca.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFinanceira)).EndInit();
            this.gpbInformacoes.ResumeLayout(false);
            this.gpbInformacoes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgImagem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gpbBusca;
        private System.Windows.Forms.DateTimePicker dtpFim;
        private System.Windows.Forms.DateTimePicker dtpInicio;
        private System.Windows.Forms.GroupBox gpbInformacoes;
        private System.Windows.Forms.PictureBox imgImagem;
        private System.Windows.Forms.Label lblOBS;
        private System.Windows.Forms.RichTextBox rtxtOBS;
        private System.Windows.Forms.DataGridView dgvFinanceira;
        private System.Windows.Forms.Button btnConsultar;
        private System.Windows.Forms.Label lblFim;
        private System.Windows.Forms.Label lblInicio;
        private System.Windows.Forms.Button btnVoltar;
        private System.Windows.Forms.DataGridViewTextBoxColumn data;
        private System.Windows.Forms.DataGridViewTextBoxColumn saida;
        private System.Windows.Forms.DataGridViewTextBoxColumn valor;
    }
}
