﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Forms;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_Financeiro
{
    public partial class frmFluxoCaixa : UserControl
    {
        public frmFluxoCaixa()
        {
            InitializeComponent();
            CarregarGrid(dtpInicio.Value.Date, dtpFim.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59));
            
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }

        private void gpbBusca_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.ForestGreen);
        }

        private void DrawGroupBox(GroupBox box, Graphics g, Color borderColor)
        {
            if (box != null)
            {
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);

                // Drawing Border
                //Left
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Right
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Bottom
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Top1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Top2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }
        }
        //Com os valores passamos por parâmetro, é só chamar o método de consultar
        private void CarregarGrid(DateTime comeco, DateTime fim)
        {
            //Instacio a classe VIEW Database
            VIEW_FinanceiroDatabase db = new VIEW_FinanceiroDatabase();

            //Chamo o método de consultar passando o valor encontrado no paramêtro
            List<VIEW_FinanceiroDTO> itens = db.Consultar(comeco, fim);

            //Deixar a GRIED gerar colunas autogamicamente? -EU VOTO NÃO!
            dgvFinanceira.AutoGenerateColumns = false;

            //Passando os valores encontrado no método de consultar para griedview. 
            dgvFinanceira.DataSource = itens;
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            //Passando o valor para o método
            CarregarGrid(dtpInicio.Value.Date, dtpFim.Value.Date.AddHours(23).AddMinutes(59).AddSeconds(59));
            //Note que está havendo uma formatação, pois por ser Datetime, por padrão, ele pega tanto a data quanto os minutos...
            //Mas nós só queremos o dia, então no primeiro campo, utilizaremos apenas o método método DATE.
            //Já no segundo paramêtro, iremos utilizar o método DATE, juntamente com a formatação de possíveis entradas de...
            //Horas, minutos e segundos respectivamente.
        }

        //Quando a grid terminar de ser carregada esse evento será chamado!
        private void dgvFinanceira_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            //Para cada item encontrado em cada linha do nosso fluxo de caixa
            foreach (DataGridViewRow item in dgvFinanceira.Rows)
            {
                //Se o campo saída da GRID conter o valor ENTRADA
                if (item.Cells[1].Value.ToString() == "Entrada")
                {
                    //As celulas que representam a operação
                    item.Cells[1].Style.BackColor = Color.DarkGreen;
                    item.Cells[1].Style.ForeColor = Color.White;

                    //As celulas que representam o valor total 
                    item.Cells[2].Style.BackColor = Color.DarkGreen;
                    item.Cells[2].Style.ForeColor = Color.White;
                }

                //Se o campo saída da GRID conter o valor SAIDA
                if (item.Cells[1].Value.ToString() == "Saída")
                {
                    //As celulas que representam a operação
                    item.Cells[1].Style.BackColor = Color.Maroon;
                    item.Cells[1].Style.ForeColor = Color.White;

                    //As celulas que representam o valor total
                    item.Cells[2].Style.BackColor = Color.Maroon;
                    item.Cells[2].Style.ForeColor = Color.White;
                }
            }
        }
    }
}
