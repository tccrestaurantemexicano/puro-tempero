﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Acesso.Funcionário;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_de_Acesso.Business;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_de_Acesso.DTO;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Funcionário
{
    public partial class frmConsultar_Funcionario : UserControl
    {
        public frmConsultar_Funcionario()
        {
            InitializeComponent();
            PermitirAcesso();
            dgvFuncionario.AutoGenerateColumns = false;

        }

        private void PermitirAcesso ()
        {
            if (UserSession.Logado.Remover == false)
            {
                ColumnR.Visible = false;
            }
            if (UserSession.Logado.Alterar == false)
            {
                ColumnA.Visible = false;
            }
        }

        private void gpbBuscar_Paint_1(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.ForestGreen);
        }

        public void CarregarGrid ()
        {
            DTO_Funcionario dto = new DTO_Funcionario();

            dto.Nome = txtNome.Text.Trim();
            if (txtCPF.Text == "         /")
            {
                dto.CPF = "";
            }
            else
            {
                dto.CPF = txtCPF.Text;
            }
            if (txtRG.Text == "  .   .   -")
            {
                dto.RG = "";
            }
            else
            {
                dto.RG = txtRG.Text;
            }
            Business_Funcionario business = new Business_Funcionario();
            List<DTO_Funcionario> lista = business.Consultar(dto);

            dgvFuncionario.AutoGenerateColumns = false;
            dgvFuncionario.DataSource = lista;
        }
        private void DrawGroupBox(GroupBox box, Graphics g, Color borderColor)
        {
            if (box != null)
            {
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);


                // Drawing Border
                //Left
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Right
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Bottom
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Top1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Top2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }
        }

        private void gpbInformações_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.DarkGreen);
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void dgvFuncionario_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex == 7)
            {
                DTO_Funcionario dto = dgvFuncionario.CurrentRow.DataBoundItem as DTO_Funcionario;
                frmAlterar_Funcionario frm = new frmAlterar_Funcionario();
                frm.Loadscreen(dto, dto.Id);

                //Faltava só isso para abrir, pois essas telas não são FORMS.
                frmMenu.Atual.OpenScreen(frm);
            }
            if(e.ColumnIndex == 8)
            {
                DTO_Funcionario dto = dgvFuncionario.CurrentRow.DataBoundItem as DTO_Funcionario;
                DialogResult resultado = MessageBox.Show("Deseja realmente excluir?", "Puro-Tempero", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (resultado == DialogResult.Yes)
                {
                    // Remover Acesso(antes do funcionario por causa da pk)

                    DTO_Acesso dto_acesso = new DTO_Acesso();
                    dto_acesso.ID_Funcionario = dto.Id;

                    Business_Acesso b = new Business_Acesso();
                    b.Remover(dto_acesso);

                    //Remover Funcionário

                    Business_Funcionario business = new Business_Funcionario();
                    business.Remover(dto);

                    CarregarGrid();
                    //Falta um método para carregar a grid quando o registro for apagado!
                }
            }
        }

        private void frmConsultar_Funcionario_Load(object sender, EventArgs e)
        {

        }
    }
}
