﻿namespace Nsf._2018.ProjetoIntegrador.PuroTempero
{
    partial class frmGerenciar_Despesas
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGerenciar_Despesas));
            this.pnlLadoDireito = new System.Windows.Forms.Panel();
            this.pnlLadoEsquerdo = new System.Windows.Forms.Panel();
            this.pnlCima = new System.Windows.Forms.Panel();
            this.pnlBaixo = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnAtualizar = new System.Windows.Forms.Button();
            this.lblGerenciamentoDespesas = new System.Windows.Forms.Label();
            this.btnDeletar = new System.Windows.Forms.Button();
            this.btnAdicionarDespesas = new System.Windows.Forms.Button();
            this.pnlCentro = new System.Windows.Forms.Panel();
            this.dgvDepesas = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel6.SuspendLayout();
            this.pnlCentro.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDepesas)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlLadoDireito
            // 
            this.pnlLadoDireito.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pnlLadoDireito.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlLadoDireito.Location = new System.Drawing.Point(883, 10);
            this.pnlLadoDireito.Name = "pnlLadoDireito";
            this.pnlLadoDireito.Size = new System.Drawing.Size(10, 579);
            this.pnlLadoDireito.TabIndex = 12;
            // 
            // pnlLadoEsquerdo
            // 
            this.pnlLadoEsquerdo.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pnlLadoEsquerdo.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLadoEsquerdo.Location = new System.Drawing.Point(0, 10);
            this.pnlLadoEsquerdo.Name = "pnlLadoEsquerdo";
            this.pnlLadoEsquerdo.Size = new System.Drawing.Size(10, 579);
            this.pnlLadoEsquerdo.TabIndex = 9;
            // 
            // pnlCima
            // 
            this.pnlCima.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pnlCima.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlCima.Location = new System.Drawing.Point(0, 0);
            this.pnlCima.Name = "pnlCima";
            this.pnlCima.Size = new System.Drawing.Size(893, 10);
            this.pnlCima.TabIndex = 10;
            // 
            // pnlBaixo
            // 
            this.pnlBaixo.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pnlBaixo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlBaixo.Location = new System.Drawing.Point(0, 589);
            this.pnlBaixo.Name = "pnlBaixo";
            this.pnlBaixo.Size = new System.Drawing.Size(893, 10);
            this.pnlBaixo.TabIndex = 11;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.panel6.Controls.Add(this.btnAtualizar);
            this.panel6.Controls.Add(this.lblGerenciamentoDespesas);
            this.panel6.Controls.Add(this.btnDeletar);
            this.panel6.Controls.Add(this.btnAdicionarDespesas);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(10, 10);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(873, 63);
            this.panel6.TabIndex = 13;
            // 
            // btnAtualizar
            // 
            this.btnAtualizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAtualizar.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnAtualizar.FlatAppearance.BorderSize = 0;
            this.btnAtualizar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(211)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.btnAtualizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAtualizar.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAtualizar.ForeColor = System.Drawing.Color.White;
            this.btnAtualizar.Image = ((System.Drawing.Image)(resources.GetObject("btnAtualizar.Image")));
            this.btnAtualizar.Location = new System.Drawing.Point(278, 0);
            this.btnAtualizar.Name = "btnAtualizar";
            this.btnAtualizar.Size = new System.Drawing.Size(123, 63);
            this.btnAtualizar.TabIndex = 16;
            this.btnAtualizar.Text = "Atualizar";
            this.btnAtualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAtualizar.UseVisualStyleBackColor = true;
            // 
            // lblGerenciamentoDespesas
            // 
            this.lblGerenciamentoDespesas.AutoSize = true;
            this.lblGerenciamentoDespesas.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGerenciamentoDespesas.ForeColor = System.Drawing.Color.White;
            this.lblGerenciamentoDespesas.Location = new System.Drawing.Point(511, 21);
            this.lblGerenciamentoDespesas.Name = "lblGerenciamentoDespesas";
            this.lblGerenciamentoDespesas.Size = new System.Drawing.Size(232, 19);
            this.lblGerenciamentoDespesas.TabIndex = 14;
            this.lblGerenciamentoDespesas.Text = "Gerenciamento de despesas";
            // 
            // btnDeletar
            // 
            this.btnDeletar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDeletar.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnDeletar.FlatAppearance.BorderSize = 0;
            this.btnDeletar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeletar.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeletar.ForeColor = System.Drawing.Color.White;
            this.btnDeletar.Image = ((System.Drawing.Image)(resources.GetObject("btnDeletar.Image")));
            this.btnDeletar.Location = new System.Drawing.Point(155, 0);
            this.btnDeletar.Name = "btnDeletar";
            this.btnDeletar.Size = new System.Drawing.Size(123, 63);
            this.btnDeletar.TabIndex = 12;
            this.btnDeletar.Text = "Deletar";
            this.btnDeletar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDeletar.UseVisualStyleBackColor = true;
            // 
            // btnAdicionarDespesas
            // 
            this.btnAdicionarDespesas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAdicionarDespesas.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnAdicionarDespesas.FlatAppearance.BorderSize = 0;
            this.btnAdicionarDespesas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdicionarDespesas.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdicionarDespesas.ForeColor = System.Drawing.Color.White;
            this.btnAdicionarDespesas.Image = ((System.Drawing.Image)(resources.GetObject("btnAdicionarDespesas.Image")));
            this.btnAdicionarDespesas.Location = new System.Drawing.Point(0, 0);
            this.btnAdicionarDespesas.Name = "btnAdicionarDespesas";
            this.btnAdicionarDespesas.Size = new System.Drawing.Size(155, 63);
            this.btnAdicionarDespesas.TabIndex = 11;
            this.btnAdicionarDespesas.Text = "  Add Despesa";
            this.btnAdicionarDespesas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAdicionarDespesas.UseVisualStyleBackColor = true;
            this.btnAdicionarDespesas.Click += new System.EventHandler(this.button4_Click);
            // 
            // pnlCentro
            // 
            this.pnlCentro.Controls.Add(this.dgvDepesas);
            this.pnlCentro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCentro.Location = new System.Drawing.Point(10, 73);
            this.pnlCentro.Name = "pnlCentro";
            this.pnlCentro.Size = new System.Drawing.Size(873, 516);
            this.pnlCentro.TabIndex = 14;
            // 
            // dgvDepesas
            // 
            this.dgvDepesas.AllowUserToAddRows = false;
            this.dgvDepesas.AllowUserToDeleteRows = false;
            this.dgvDepesas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDepesas.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvDepesas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvDepesas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDepesas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.dgvDepesas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDepesas.Location = new System.Drawing.Point(0, 0);
            this.dgvDepesas.Name = "dgvDepesas";
            this.dgvDepesas.ReadOnly = true;
            this.dgvDepesas.RowHeadersVisible = false;
            this.dgvDepesas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDepesas.Size = new System.Drawing.Size(873, 516);
            this.dgvDepesas.TabIndex = 1;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Nome Despesa";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Preço";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Descrição";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // frmGerenciar_Despesas
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.pnlCentro);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.pnlLadoDireito);
            this.Controls.Add(this.pnlLadoEsquerdo);
            this.Controls.Add(this.pnlCima);
            this.Controls.Add(this.pnlBaixo);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "frmGerenciar_Despesas";
            this.Size = new System.Drawing.Size(893, 599);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.pnlCentro.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDepesas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlLadoDireito;
        private System.Windows.Forms.Panel pnlLadoEsquerdo;
        private System.Windows.Forms.Panel pnlCima;
        private System.Windows.Forms.Panel pnlBaixo;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label lblGerenciamentoDespesas;
        private System.Windows.Forms.Button btnDeletar;
        private System.Windows.Forms.Button btnAdicionarDespesas;
        private System.Windows.Forms.Button btnAtualizar;
        private System.Windows.Forms.Panel pnlCentro;
        private System.Windows.Forms.DataGridView dgvDepesas;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
    }
}
