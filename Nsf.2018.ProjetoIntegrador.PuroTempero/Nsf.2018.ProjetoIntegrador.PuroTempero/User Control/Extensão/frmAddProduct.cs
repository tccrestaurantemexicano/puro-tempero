﻿using Nsf.PuroTempero.FERRAMENTAS.Código_Barra;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero
{
    public partial class frmAddProduct : Form
    {
        public frmAddProduct()
        {
            InitializeComponent();
        }


        private void btnLimpar_Click(object sender, EventArgs e)
        {
            foreach (Control controles in this.Controls)    //Para cada controle dentro do meu Formulário
            {
                if (controles is TextBox)                   //Se o controle for uma Textbox                                        
                    ((TextBox)controles).Clear();           //É pego todas as Textbox e depois é chamada a função Clear

                if (controles is RichTextBox)               //Se o controle for uma RichTextbox                                                               
                    ((RichTextBox)controles).Clear();       //É pego todas as RichTextbox e depois é chamada a função Clear    

            }
        }

        private void chkCodBarra_Click(object sender, EventArgs e)
        {
            if (chkCodBarra.Checked == true)
            {
                SaveFileDialog janela = new SaveFileDialog();   //Solicita para que o usuário salve um determinado arquivo em um determinado local
                janela.Filter = "Imagens (*.png)|*.png";        //Filtra apenas pelas imagens em PNG encontradas na pasta Imagens

                DialogResult result = janela.ShowDialog();      //Mostra uma caixa de dialogo para o usuário

                if (result == DialogResult.OK)                  //Se for OK
                {
                    BarCodeService barcode = new BarCodeService();                                  //Instancio as minhas ferramentas                  
                    Image image = barcode.SalvarCodigoBarra(txtCodBarra.Text, janela.FileName);     //Com um retorno, eu chamo e passa os valores para o Método

                    imgCodBarra.Image = image;                                                      //Passo o valor gerado para PictureBox
                }

            }
        }

        private void label12_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void btnMais_Click(object sender, EventArgs e)
        {
            frmAdicionar_Categoria add = new frmAdicionar_Categoria();
            add.ShowDialog();
        }
    }
}
