﻿namespace Nsf._2018.ProjetoIntegrador.PuroTempero
{
    partial class frmConsultar_Vendas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConsultar_Vendas));
            this.pnlLadoEsquerdo = new System.Windows.Forms.Panel();
            this.pnlCima = new System.Windows.Forms.Panel();
            this.pnlLadoDireito = new System.Windows.Forms.Panel();
            this.PnlBaixo = new System.Windows.Forms.Panel();
            this.lblPreco = new System.Windows.Forms.Label();
            this.lblValorRecebido = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblRegistroVendas = new System.Windows.Forms.Label();
            this.btnAtualizar = new System.Windows.Forms.Button();
            this.pnlCentral = new System.Windows.Forms.Panel();
            this.dgvVenda = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PnlBaixo.SuspendLayout();
            this.panel5.SuspendLayout();
            this.pnlCentral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVenda)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlLadoEsquerdo
            // 
            this.pnlLadoEsquerdo.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pnlLadoEsquerdo.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLadoEsquerdo.Location = new System.Drawing.Point(0, 10);
            this.pnlLadoEsquerdo.Name = "pnlLadoEsquerdo";
            this.pnlLadoEsquerdo.Size = new System.Drawing.Size(10, 558);
            this.pnlLadoEsquerdo.TabIndex = 5;
            // 
            // pnlCima
            // 
            this.pnlCima.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pnlCima.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlCima.Location = new System.Drawing.Point(0, 0);
            this.pnlCima.Name = "pnlCima";
            this.pnlCima.Size = new System.Drawing.Size(883, 10);
            this.pnlCima.TabIndex = 6;
            // 
            // pnlLadoDireito
            // 
            this.pnlLadoDireito.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pnlLadoDireito.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlLadoDireito.Location = new System.Drawing.Point(883, 0);
            this.pnlLadoDireito.Name = "pnlLadoDireito";
            this.pnlLadoDireito.Size = new System.Drawing.Size(10, 568);
            this.pnlLadoDireito.TabIndex = 7;
            // 
            // PnlBaixo
            // 
            this.PnlBaixo.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.PnlBaixo.Controls.Add(this.lblPreco);
            this.PnlBaixo.Controls.Add(this.lblValorRecebido);
            this.PnlBaixo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PnlBaixo.Location = new System.Drawing.Point(0, 568);
            this.PnlBaixo.Name = "PnlBaixo";
            this.PnlBaixo.Size = new System.Drawing.Size(893, 31);
            this.PnlBaixo.TabIndex = 8;
            // 
            // lblPreco
            // 
            this.lblPreco.AutoSize = true;
            this.lblPreco.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreco.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.lblPreco.Location = new System.Drawing.Point(189, 5);
            this.lblPreco.Name = "lblPreco";
            this.lblPreco.Size = new System.Drawing.Size(66, 19);
            this.lblPreco.TabIndex = 14;
            this.lblPreco.Text = "R$: 0,00";
            // 
            // lblValorRecebido
            // 
            this.lblValorRecebido.AutoSize = true;
            this.lblValorRecebido.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorRecebido.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.lblValorRecebido.Location = new System.Drawing.Point(50, 5);
            this.lblValorRecebido.Name = "lblValorRecebido";
            this.lblValorRecebido.Size = new System.Drawing.Size(133, 19);
            this.lblValorRecebido.TabIndex = 14;
            this.lblValorRecebido.Text = "Valor Recebido:";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.panel5.Controls.Add(this.lblRegistroVendas);
            this.panel5.Controls.Add(this.btnAtualizar);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(10, 10);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(873, 63);
            this.panel5.TabIndex = 10;
            // 
            // lblRegistroVendas
            // 
            this.lblRegistroVendas.AutoSize = true;
            this.lblRegistroVendas.Font = new System.Drawing.Font("Century Gothic", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegistroVendas.ForeColor = System.Drawing.Color.White;
            this.lblRegistroVendas.Location = new System.Drawing.Point(417, 17);
            this.lblRegistroVendas.Name = "lblRegistroVendas";
            this.lblRegistroVendas.Size = new System.Drawing.Size(220, 26);
            this.lblRegistroVendas.TabIndex = 14;
            this.lblRegistroVendas.Text = "Registro de Vendas";
            // 
            // btnAtualizar
            // 
            this.btnAtualizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAtualizar.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnAtualizar.FlatAppearance.BorderSize = 0;
            this.btnAtualizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAtualizar.Font = new System.Drawing.Font("Century Gothic", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAtualizar.ForeColor = System.Drawing.Color.White;
            this.btnAtualizar.Image = ((System.Drawing.Image)(resources.GetObject("btnAtualizar.Image")));
            this.btnAtualizar.Location = new System.Drawing.Point(0, 0);
            this.btnAtualizar.Name = "btnAtualizar";
            this.btnAtualizar.Size = new System.Drawing.Size(155, 63);
            this.btnAtualizar.TabIndex = 11;
            this.btnAtualizar.Text = "  Atualizar";
            this.btnAtualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAtualizar.UseVisualStyleBackColor = true;
            // 
            // pnlCentral
            // 
            this.pnlCentral.Controls.Add(this.dgvVenda);
            this.pnlCentral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCentral.Location = new System.Drawing.Point(10, 73);
            this.pnlCentral.Name = "pnlCentral";
            this.pnlCentral.Size = new System.Drawing.Size(873, 495);
            this.pnlCentral.TabIndex = 11;
            // 
            // dgvVenda
            // 
            this.dgvVenda.AllowUserToAddRows = false;
            this.dgvVenda.AllowUserToDeleteRows = false;
            this.dgvVenda.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvVenda.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvVenda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVenda.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5});
            this.dgvVenda.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvVenda.Location = new System.Drawing.Point(0, 0);
            this.dgvVenda.Name = "dgvVenda";
            this.dgvVenda.ReadOnly = true;
            this.dgvVenda.RowHeadersVisible = false;
            this.dgvVenda.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVenda.Size = new System.Drawing.Size(873, 495);
            this.dgvVenda.TabIndex = 0;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "ID Venda";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Valor Líquido";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Desconto Líquido";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Valor Total";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Data";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // frmConsultar_Vendas
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(893, 599);
            this.Controls.Add(this.pnlCentral);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.pnlLadoEsquerdo);
            this.Controls.Add(this.pnlCima);
            this.Controls.Add(this.pnlLadoDireito);
            this.Controls.Add(this.PnlBaixo);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmConsultar_Vendas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Puro Tempero | Consultar Vendas";
            this.PnlBaixo.ResumeLayout(false);
            this.PnlBaixo.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.pnlCentral.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVenda)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlLadoEsquerdo;
        private System.Windows.Forms.Panel pnlCima;
        private System.Windows.Forms.Panel pnlLadoDireito;
        private System.Windows.Forms.Panel PnlBaixo;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lblRegistroVendas;
        private System.Windows.Forms.Button btnAtualizar;
        private System.Windows.Forms.Panel pnlCentral;
        private System.Windows.Forms.DataGridView dgvVenda;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.Label lblPreco;
        private System.Windows.Forms.Label lblValorRecebido;
    }
}