﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras.Business;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Ferramentas;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Ferramentas.Imagem;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Compras.Produto
{
    public partial class frmCadastrar_Produto : UserControl
    {

        Business_ProdutoCompra db = new Business_ProdutoCompra();

        public frmCadastrar_Produto()
        {
            InitializeComponent();
            CarregarCombos();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }
        private void CarregarCombos ()
        {
            Business_Fornecedor db = new Business_Fornecedor();
            List<DTO_Fornecedor> list = db.Listar();

            cboFornecedor.ValueMember = nameof(DTO_Fornecedor.ID);
            cboFornecedor.DisplayMember = nameof(DTO_Fornecedor.Razao_Social);

            cboFornecedor.DataSource = list;
        }

        private void gbpProduto_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.ForestGreen);
        }

        private void DrawGroupBox(GroupBox box, Graphics g, Color borderColor)
        {
            if (box != null)
            {
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);

                // Drawing Border
                //Left
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Right
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Bottom
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Top1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Top2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }
        }

        public void Salvar(DTO_ProdutoCompra dto)
        {
            //Pegando o DTO do Fornecedor
            DTO_Fornecedor fornecedor = cboFornecedor.SelectedItem as DTO_Fornecedor;

            //Passo o valor dos controles para o DTO.
            dto.ID_Fornecedor = fornecedor.ID;
            dto.Produto = txtProduto.Text;
            dto.Preco = nudPreco.Value;
            dto.Marca = txtMarca.Text;
            dto.Imagem = ImagemPlugin.ConverterParaString(imgGIF.Image);

            //Passo o valor do DTO para business.
            db.Salvar(dto);

            //Trocando as propriedades da label
            lblMensagem.ForeColor = Color.White;
            lblMensagem.BackColor = Color.ForestGreen;

            //Mostrando o valor para o usuário.
            lblMensagem.Text = "Produto Salvo com Sucesso!";
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            //Passo o valor do DTO para o método Salvar
            DTO_ProdutoCompra dto = new DTO_ProdutoCompra();
            Salvar(dto);
        }

        private void btnProcurar_Click(object sender, EventArgs e)
        {
            //Instâncio a função que irá abrir os documentos Windows/Desktop.
            OpenFileDialog dialog = new OpenFileDialog();

            //Faz um filtro apenas em JPAG ou PNG
            dialog.Filter = "JPG Files(*.jpg)|*.jpg|PNG Files(*.png)|*.png";

            //Habilita a caixa de arquivos Windows.
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                //Pega a localização da imagem
                imgGIF.ImageLocation = dialog.FileName;
            }
        }

        private void lblProduto_Click(object sender, EventArgs e)
        {

        }
    }
}
