﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Ferramentas.Localização;
using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_Compras.Business;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Compras.Fornecedores
{
    public partial class frmAlterar_Fornecedor : UserControl
    {
        public frmAlterar_Fornecedor()
        {
            InitializeComponent();

            Localizacao uf = new Localizacao();
            cboUF.DataSource = uf.UF();
        }

        private void gpbEndereco_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.ForestGreen);
        }
        private void DrawGroupBox(GroupBox box, Graphics g, Color borderColor)
        {
            if (box != null)
            {
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);

                // Drawing Border
                //Left
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Right
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Bottom
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Top1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Top2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }
        }


        DTO_Fornecedor dto;

        public void LoadScreen (DTO_Fornecedor dto)
        {
            this.dto = dto;

            lblID.Text = dto.ID.ToString();
            txtNome.Text = dto.Razao_Social;
            txtCNPJ.Text = dto.CNPJ;
            txtTelefone.Text = dto.Telefone;
            txtEmail.Text = dto.Email;
            txtCEP.Text = dto.CEP;
            txtEndereco.Text = dto.Endereco;
            txtComplemento.Text = dto.Complemento;
            nudNumero.Value = dto.Numero_Casa;
            cboUF.SelectedItem = dto.UF;
            txtCidade.Text = dto.Cidade;

        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            frmConsultar_Fornecedor tela = new frmConsultar_Fornecedor();
            frmMenu.Atual.OpenScreen(tela);
            
        }
        public void Alterar (DTO_Fornecedor dto)
        {
            dto.Razao_Social = txtNome.Text;
            dto.CNPJ = txtCNPJ.Text;
            dto.Telefone = txtTelefone.Text;
            dto.Email = txtEmail.Text;
            dto.CEP = txtCEP.Text;
            dto.Endereco = txtEndereco.Text;
            dto.Complemento = txtComplemento.Text;
            dto.Numero_Casa = nudNumero.Value;
            dto.UF = cboUF.SelectedItem.ToString();
            dto.Cidade = txtCidade.Text;

            Business_Fornecedor db = new Business_Fornecedor();
            db.Alterar(dto);

            lblMensagem.BackColor = Color.ForestGreen;
            lblMensagem.ForeColor = Color.White;
            lblMensagem.Text = "Registro alterado com sucesso!!";
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            Alterar(dto);
        }

    }
}
