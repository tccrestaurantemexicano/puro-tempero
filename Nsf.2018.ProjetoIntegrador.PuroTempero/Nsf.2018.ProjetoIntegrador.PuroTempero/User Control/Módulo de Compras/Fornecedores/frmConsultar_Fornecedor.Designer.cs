﻿namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Compras.Fornecedores
{
    partial class frmConsultar_Fornecedor
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvCompra = new System.Windows.Forms.DataGridView();
            this.btnVoltar = new System.Windows.Forms.Button();
            this.gpbBuscar = new System.Windows.Forms.GroupBox();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.lblNome = new System.Windows.Forms.Label();
            this.gpbInformações = new System.Windows.Forms.GroupBox();
            this.lblOBS = new System.Windows.Forms.Label();
            this.rtxtInformacao = new System.Windows.Forms.RichTextBox();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn2 = new System.Windows.Forms.DataGridViewImageColumn();
            this.Nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CNPJ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Telefone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cidade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewImageColumn3 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn4 = new System.Windows.Forms.DataGridViewImageColumn();
            this.ColumnA = new System.Windows.Forms.DataGridViewImageColumn();
            this.ColumnR = new System.Windows.Forms.DataGridViewImageColumn();
            this.imgImagem = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCompra)).BeginInit();
            this.gpbBuscar.SuspendLayout();
            this.gpbInformações.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgImagem)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvCompra
            // 
            this.dgvCompra.AllowUserToAddRows = false;
            this.dgvCompra.AllowUserToDeleteRows = false;
            this.dgvCompra.AllowUserToResizeColumns = false;
            this.dgvCompra.AllowUserToResizeRows = false;
            this.dgvCompra.BackgroundColor = System.Drawing.Color.Snow;
            this.dgvCompra.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvCompra.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            this.dgvCompra.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvCompra.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCompra.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nome,
            this.CNPJ,
            this.Telefone,
            this.Cidade,
            this.ColumnA,
            this.ColumnR});
            this.dgvCompra.GridColor = System.Drawing.Color.Snow;
            this.dgvCompra.Location = new System.Drawing.Point(15, 99);
            this.dgvCompra.MultiSelect = false;
            this.dgvCompra.Name = "dgvCompra";
            this.dgvCompra.ReadOnly = true;
            this.dgvCompra.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvCompra.RowHeadersVisible = false;
            this.dgvCompra.Size = new System.Drawing.Size(393, 288);
            this.dgvCompra.TabIndex = 24;
            this.dgvCompra.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCompra_CellContentClick);
            // 
            // btnVoltar
            // 
            this.btnVoltar.BackColor = System.Drawing.Color.Maroon;
            this.btnVoltar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnVoltar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVoltar.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.btnVoltar.ForeColor = System.Drawing.Color.White;
            this.btnVoltar.Location = new System.Drawing.Point(529, 403);
            this.btnVoltar.Name = "btnVoltar";
            this.btnVoltar.Size = new System.Drawing.Size(75, 28);
            this.btnVoltar.TabIndex = 42;
            this.btnVoltar.Text = "Voltar";
            this.btnVoltar.UseVisualStyleBackColor = false;
            this.btnVoltar.Click += new System.EventHandler(this.btnVoltar_Click);
            // 
            // gpbBuscar
            // 
            this.gpbBuscar.Controls.Add(this.dgvCompra);
            this.gpbBuscar.Controls.Add(this.txtNome);
            this.gpbBuscar.Controls.Add(this.lblNome);
            this.gpbBuscar.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold);
            this.gpbBuscar.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbBuscar.Location = new System.Drawing.Point(4, -5);
            this.gpbBuscar.Name = "gpbBuscar";
            this.gpbBuscar.Size = new System.Drawing.Size(421, 405);
            this.gpbBuscar.TabIndex = 41;
            this.gpbBuscar.TabStop = false;
            this.gpbBuscar.Text = "Buscar ";
            this.gpbBuscar.Paint += new System.Windows.Forms.PaintEventHandler(this.gpbBuscar_Paint);
            // 
            // txtNome
            // 
            this.txtNome.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNome.ForeColor = System.Drawing.Color.Black;
            this.txtNome.Location = new System.Drawing.Point(15, 56);
            this.txtNome.MaxLength = 50;
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(393, 22);
            this.txtNome.TabIndex = 0;
            this.txtNome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNome_KeyPress);
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.lblNome.ForeColor = System.Drawing.Color.Black;
            this.lblNome.Location = new System.Drawing.Point(12, 37);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(95, 16);
            this.lblNome.TabIndex = 17;
            this.lblNome.Text = "Fornecedor:";
            // 
            // gpbInformações
            // 
            this.gpbInformações.Controls.Add(this.lblOBS);
            this.gpbInformações.Controls.Add(this.imgImagem);
            this.gpbInformações.Controls.Add(this.rtxtInformacao);
            this.gpbInformações.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbInformações.ForeColor = System.Drawing.Color.DarkGreen;
            this.gpbInformações.Location = new System.Drawing.Point(431, -5);
            this.gpbInformações.Name = "gpbInformações";
            this.gpbInformações.Size = new System.Drawing.Size(173, 405);
            this.gpbInformações.TabIndex = 43;
            this.gpbInformações.TabStop = false;
            this.gpbInformações.Text = "Informações ";
            this.gpbInformações.Paint += new System.Windows.Forms.PaintEventHandler(this.gpbBuscar_Paint);
            // 
            // lblOBS
            // 
            this.lblOBS.AutoSize = true;
            this.lblOBS.ForeColor = System.Drawing.Color.Black;
            this.lblOBS.Location = new System.Drawing.Point(5, 224);
            this.lblOBS.Name = "lblOBS";
            this.lblOBS.Size = new System.Drawing.Size(41, 20);
            this.lblOBS.TabIndex = 30;
            this.lblOBS.Text = "OBS:";
            // 
            // rtxtInformacao
            // 
            this.rtxtInformacao.Enabled = false;
            this.rtxtInformacao.Font = new System.Drawing.Font("Arial", 10F);
            this.rtxtInformacao.Location = new System.Drawing.Point(5, 247);
            this.rtxtInformacao.Name = "rtxtInformacao";
            this.rtxtInformacao.Size = new System.Drawing.Size(161, 140);
            this.rtxtInformacao.TabIndex = 27;
            this.rtxtInformacao.Text = "Essa tela serve para fazer a consulta de todos os fornecedors contidos no program" +
    "a. Já é incluido o cálculo com o intuito de fazer a verificação dos produtos em " +
    "estoque.";
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.ReadOnly = true;
            // 
            // dataGridViewImageColumn2
            // 
            this.dataGridViewImageColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.dataGridViewImageColumn2.HeaderText = "";
            this.dataGridViewImageColumn2.Name = "dataGridViewImageColumn2";
            this.dataGridViewImageColumn2.ReadOnly = true;
            // 
            // Nome
            // 
            this.Nome.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Nome.DataPropertyName = "Razao_Social";
            this.Nome.HeaderText = "Empresa";
            this.Nome.Name = "Nome";
            this.Nome.ReadOnly = true;
            // 
            // CNPJ
            // 
            this.CNPJ.DataPropertyName = "CNPJ";
            this.CNPJ.HeaderText = "CNPJ";
            this.CNPJ.Name = "CNPJ";
            this.CNPJ.ReadOnly = true;
            // 
            // Telefone
            // 
            this.Telefone.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Telefone.DataPropertyName = "Telefone";
            this.Telefone.HeaderText = "Telefone";
            this.Telefone.Name = "Telefone";
            this.Telefone.ReadOnly = true;
            // 
            // Cidade
            // 
            this.Cidade.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Cidade.DataPropertyName = "Endereco";
            this.Cidade.HeaderText = "Endereco";
            this.Cidade.Name = "Cidade";
            this.Cidade.ReadOnly = true;
            // 
            // dataGridViewImageColumn3
            // 
            this.dataGridViewImageColumn3.HeaderText = "";
            this.dataGridViewImageColumn3.Image = global::Nsf._2018.ProjetoIntegrador.PuroTempero.Properties.Resources.Editar;
            this.dataGridViewImageColumn3.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.dataGridViewImageColumn3.Name = "dataGridViewImageColumn3";
            this.dataGridViewImageColumn3.ReadOnly = true;
            this.dataGridViewImageColumn3.Width = 25;
            // 
            // dataGridViewImageColumn4
            // 
            this.dataGridViewImageColumn4.HeaderText = "";
            this.dataGridViewImageColumn4.Image = global::Nsf._2018.ProjetoIntegrador.PuroTempero.Properties.Resources.Remove;
            this.dataGridViewImageColumn4.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.dataGridViewImageColumn4.Name = "dataGridViewImageColumn4";
            this.dataGridViewImageColumn4.ReadOnly = true;
            this.dataGridViewImageColumn4.Width = 25;
            // 
            // ColumnA
            // 
            this.ColumnA.HeaderText = "";
            this.ColumnA.Image = global::Nsf._2018.ProjetoIntegrador.PuroTempero.Properties.Resources.Editar;
            this.ColumnA.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.ColumnA.Name = "ColumnA";
            this.ColumnA.ReadOnly = true;
            this.ColumnA.Width = 25;
            // 
            // ColumnR
            // 
            this.ColumnR.HeaderText = "";
            this.ColumnR.Image = global::Nsf._2018.ProjetoIntegrador.PuroTempero.Properties.Resources.Remove;
            this.ColumnR.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.ColumnR.Name = "ColumnR";
            this.ColumnR.ReadOnly = true;
            this.ColumnR.Width = 25;
            // 
            // imgImagem
            // 
            this.imgImagem.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.imgImagem.Image = global::Nsf._2018.ProjetoIntegrador.PuroTempero.Properties.Resources.Pesquisa1;
            this.imgImagem.Location = new System.Drawing.Point(5, 31);
            this.imgImagem.Name = "imgImagem";
            this.imgImagem.Size = new System.Drawing.Size(161, 188);
            this.imgImagem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgImagem.TabIndex = 29;
            this.imgImagem.TabStop = false;
            // 
            // frmConsultar_Fornecedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnVoltar);
            this.Controls.Add(this.gpbBuscar);
            this.Controls.Add(this.gpbInformações);
            this.Name = "frmConsultar_Fornecedor";
            this.Size = new System.Drawing.Size(607, 435);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCompra)).EndInit();
            this.gpbBuscar.ResumeLayout(false);
            this.gpbBuscar.PerformLayout();
            this.gpbInformações.ResumeLayout(false);
            this.gpbInformações.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgImagem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvCompra;
        private System.Windows.Forms.Button btnVoltar;
        private System.Windows.Forms.GroupBox gpbBuscar;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.GroupBox gpbInformações;
        private System.Windows.Forms.Label lblOBS;
        private System.Windows.Forms.PictureBox imgImagem;
        private System.Windows.Forms.RichTextBox rtxtInformacao;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn CNPJ;
        private System.Windows.Forms.DataGridViewTextBoxColumn Telefone;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cidade;
        private System.Windows.Forms.DataGridViewImageColumn ColumnA;
        private System.Windows.Forms.DataGridViewImageColumn ColumnR;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn3;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn4;
    }
}
