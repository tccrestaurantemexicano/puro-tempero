﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Forms;
using Nsf.PuroTempero.BUSINESS.Módulo_Compras;
using Nsf.PuroTempero.MODELO.Módulo_Compras;
using Nsf.PuroTempero.DATABASE.Módulo_Estoque;
using Nsf.PuroTempero.MODELO.Módulo_Estoque;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Estoque
{
    public partial class frmReceber_Compra : UserControl
    {
        public frmReceber_Compra()
        {
            InitializeComponent();
        }
        private void CarregarGrid()
        {
            //Pego o valor do controle
            int id = Convert.ToInt32(NudPedido.Value);

            //Chamo a Business e passo o valor encontrado no controle para o método de consultar
            VIEW_EstoqueDatabase db = new VIEW_EstoqueDatabase();
            List<VIEW_EstoqueDTO> consult = db.Consultar(id);

            //As colunas não irão gerar automagicamente
            dgvEstoque.AutoGenerateColumns = false;

            //Passo o valor do MÉTODO LISTAR para a gried
            dgvEstoque.DataSource = consult;
        }

        private void ReceberItens()
        {
            //Preparo uma LISTA de DTO com os valores contidos no objeto de ESTOQUE
            List<DTO_Estoque> estoque = new List<DTO_Estoque>();

            //Pego o valor contido no Data Gried View
            //Como é uma lista, podem aparecer diversos itens, por conta disso, utiliza-se uma LISTA
            List<VIEW_EstoqueDTO> itens = dgvEstoque.DataSource as List<VIEW_EstoqueDTO>;

            //TRADUÇÃO: Para cada VALOR encontrado no DTO referente aos ITENS DE COMPRA, atribuimos esse valor a uma variável
            //E depois passamos o valor da variável que está pegando os valores encontrados na Gried View
            foreach (VIEW_EstoqueDTO item in itens)
            {
                //Instância de DTO
                DTO_Estoque dto = new DTO_Estoque();

                //Atribuição de valores
                dto.ID_Produto = item.ID_Produto;
                dto.ID_Pedido = item.ID;

                //Adiciono na minha LISTA DE ESTOQUE
                estoque.Add(dto);
            }

            //Chama TELA CHAMA BUSINESS
            Business_Estoque db = new Business_Estoque();

            //Chama o método salvar (Sem retorno)
            db.Salvar(estoque);
            MessageBox.Show("Produto recebido com sucesso!", 
                            "Puro Tempero", 
                            MessageBoxButtons.OK, 
                            MessageBoxIcon.Information);     
        }

        private void groupBox1_Paint(object sender, PaintEventArgs e)
        {
            GroupBox box = sender as GroupBox;
            DrawGroupBox(box, e.Graphics, Color.ForestGreen);
        }

        private void DrawGroupBox(GroupBox box, Graphics g, Color borderColor)
        {
            if (box != null)
            {
                Brush borderBrush = new SolidBrush(borderColor);
                Pen borderPen = new Pen(borderBrush);
                SizeF strSize = g.MeasureString(box.Text, box.Font);
                Rectangle rect = new Rectangle(box.ClientRectangle.X,
                                               box.ClientRectangle.Y + (int)(strSize.Height / 2),
                                               box.ClientRectangle.Width - 1,
                                               box.ClientRectangle.Height - (int)(strSize.Height / 2) - 1);

                // Drawing Border
                //Left
                g.DrawLine(borderPen, rect.Location, new Point(rect.X, rect.Y + rect.Height));
                //Right
                g.DrawLine(borderPen, new Point(rect.X + rect.Width, rect.Y), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Bottom
                g.DrawLine(borderPen, new Point(rect.X, rect.Y + rect.Height), new Point(rect.X + rect.Width, rect.Y + rect.Height));
                //Top1
                g.DrawLine(borderPen, new Point(rect.X, rect.Y), new Point(rect.X + box.Padding.Left, rect.Y));
                //Top2
                g.DrawLine(borderPen, new Point(rect.X + box.Padding.Left + (int)(strSize.Width), rect.Y), new Point(rect.X + rect.Width, rect.Y));
            }
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            this.Hide();
        }

        private void btnReceberEstoque_Click(object sender, EventArgs e)
        {
            ReceberItens();
        }

        private void imgPesquisa_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }
    }
}
