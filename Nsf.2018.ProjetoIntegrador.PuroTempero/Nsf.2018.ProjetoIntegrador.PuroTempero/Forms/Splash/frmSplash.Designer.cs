﻿namespace Nsf._2018.ProjetoIntegrador.PuroTempero.Forms
{
    partial class frmSplash
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSplash));
            this.lblMexicano = new System.Windows.Forms.Label();
            this.lblRestaurante = new System.Windows.Forms.Label();
            this.imgGif = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgGif)).BeginInit();
            this.SuspendLayout();
            // 
            // lblMexicano
            // 
            this.lblMexicano.AutoSize = true;
            this.lblMexicano.BackColor = System.Drawing.Color.Maroon;
            this.lblMexicano.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMexicano.ForeColor = System.Drawing.Color.White;
            this.lblMexicano.Location = new System.Drawing.Point(80, 58);
            this.lblMexicano.Name = "lblMexicano";
            this.lblMexicano.Size = new System.Drawing.Size(152, 30);
            this.lblMexicano.TabIndex = 7;
            this.lblMexicano.Text = "Puro Tempero";
            // 
            // lblRestaurante
            // 
            this.lblRestaurante.AutoSize = true;
            this.lblRestaurante.BackColor = System.Drawing.Color.Green;
            this.lblRestaurante.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRestaurante.ForeColor = System.Drawing.Color.White;
            this.lblRestaurante.Location = new System.Drawing.Point(40, 28);
            this.lblRestaurante.Name = "lblRestaurante";
            this.lblRestaurante.Size = new System.Drawing.Size(239, 30);
            this.lblRestaurante.TabIndex = 6;
            this.lblRestaurante.Text = "Restaurante Mexicano";
            // 
            // imgGif
            // 
            this.imgGif.BackColor = System.Drawing.Color.Transparent;
            this.imgGif.Image = ((System.Drawing.Image)(resources.GetObject("imgGif.Image")));
            this.imgGif.Location = new System.Drawing.Point(3, 61);
            this.imgGif.Name = "imgGif";
            this.imgGif.Size = new System.Drawing.Size(316, 139);
            this.imgGif.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgGif.TabIndex = 4;
            this.imgGif.TabStop = false;
            // 
            // frmSplash
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Snow;
            this.ClientSize = new System.Drawing.Size(321, 201);
            this.Controls.Add(this.lblMexicano);
            this.Controls.Add(this.lblRestaurante);
            this.Controls.Add(this.imgGif);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmSplash";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            ((System.ComponentModel.ISupportInitialize)(this.imgGif)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox imgGif;
        private System.Windows.Forms.Label lblMexicano;
        private System.Windows.Forms.Label lblRestaurante;
    }
}