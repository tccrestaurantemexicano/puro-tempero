﻿using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_de_Acesso.Database;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Forms.Login;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Menu;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_RH;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.Forms
{
    public partial class frmMenu : Form
    {
        public static frmMenu Atual;
        int panelwidth;
        bool isCollapsed;
        private ContextMenuStrip contextMenuStrip;
        public frmMenu()
        {
            InitializeComponent();

            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip();
            //CarregarPermissoes();
            Atual = this;

            //Pegando o tamanho do panel
            panelwidth = pnlEsquerda.Width;
            isCollapsed = false;

            //Slide não vísivel
            pnlLado.Visible = false;

            //Começar logado
            frmHome tela = new frmHome();
            OpenScreen(tela);

            //Informações do usuário
            //LoadUser();

        }

        private void CarregarPermissoes()
        {
            //Caso ele não tenha permissão para Cadastrar
            //if (UserSession.Logado.Salvar == false)
            //{
            //    menuCadastroCliente.Enabled = false;
            //    menuCadastrarPedidoCompra.Enabled = false;
            //    menuCadastrarProdutoCompra.Enabled = false;
            //    menuCadastrarPedidoVenda.Enabled = false;
            //    menuCadastrarProdutoVenda.Enabled = false;  
            //    menuCadastroFornecedor.Enabled = false;
            //    menuCadastroFuncionario.Enabled = false;
            //    menuBaterB.Enabled = false;
            //    //menuReceberEstoque.Enabled = false;
            //}

            //Caso ele não tenha permissão para Consultar
            //if (UserSession.Logado.Consultar == false)
            //{
            //    menuConsultaCliente.Enabled = false;
            //    menuConsultarPedidoCompra.Enabled = false;
            //    menuConsultarProdutoCompra.Enabled = false;
            //    menuConsultarPedidoVenda.Enabled = false;
            //    menuConsultarProdutoVenda.Enabled = false;
            //    menuConsultarFornecedor.Enabled = false;
            //    menuConsultarEstoque.Enabled = false;
            //    menuConsultarFuncionario.Enabled = false;
            //    menuFluxo.Enabled = false;
            //    menuFolha.Enabled = false;
            //}

            //Outra coisa que eu pensei foi em relação a Deleter e alterar.
            //Basicamente, eu estou bloqueando a griedview nas telas que tem essa parte de alterar.
        }
        public void OpenScreen(UserControl control)
        {
            // Se a quantidades de CONTROLES for igual a 1, Ou seja, se já tiver uma tela dentro da GROUP BOX.
            if (pnlContainer.Controls.Count == 1)
                pnlContainer.Controls.RemoveAt(0);
            pnlContainer.Controls.Add(control);
            //Eu irei REMOVER o primeiro ÍNDICE do CONTROLE
            //Ou seja, sempre quando eu for abrir uma tela nova, ele vai remover a tela ATUAL e ADICIONA a NOVA.
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Tem certeza que deseja fechar o Programa",
                                                 "ATENÇÃO",
                                                 MessageBoxButtons.YesNo,
                                                 MessageBoxIcon.Exclamation);

            if (dialog == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void LoadUser ()
        {
            VIEW_AcessoDTO user = new VIEW_AcessoDTO();
            user = UserSession.Logado;  
            
            lblUser.Text = user.Usuario;
            //lblFunção.Text = user.cargo;
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            if (isCollapsed)
            {
                pnlEsquerda.Width = pnlEsquerda.Width + 10;

                if (pnlEsquerda.Width >= panelwidth)
                {
                    timer1.Stop();
                    isCollapsed = false;
                    this.Refresh();
                }
            }
            else
            {
                pnlEsquerda.Width = pnlEsquerda.Width - 10;

                if (pnlEsquerda.Width <= 51)
                {
                    timer1.Stop();
                    isCollapsed = true;
                    this.Refresh();
                }
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void DeslizarSlide(Control control)
        {
            pnlLado.Height = control.Height;
            pnlLado.Top = control.Top;
            pnlLado.Visible = true;
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            DeslizarSlide(btnHome);
                    
            frmHome tela = new frmHome();
            OpenScreen(tela);
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            DeslizarSlide(btnConsultar);

            frmConsultar tela = new frmConsultar();
            OpenScreen(tela);
        }

        private void btnCompra_Click(object sender, EventArgs e)
        {
            DeslizarSlide(btnCompra);

            User_Control.Menu.frmCompras tela = new User_Control.Menu.frmCompras();
            OpenScreen(tela);
        }

        private void btnVenda_Click(object sender, EventArgs e)
        {
            DeslizarSlide(btnVenda);

            User_Control.Menu.frmVendas tela = new User_Control.Menu.frmVendas();
            OpenScreen(tela);
        }

        private void btnRelatorio_Click(object sender, EventArgs e)
        {
            DeslizarSlide(btnDespesas);

            frmFolha_Pagamento tela = new frmFolha_Pagamento();
            OpenScreen(tela);
        }

        private void btnCadastro_Click(object sender, EventArgs e)
        {
            DeslizarSlide(btnCadastro);

            frmCadastro tela = new frmCadastro();
            OpenScreen(tela);
        }

        private void btnDespesas_Click(object sender, EventArgs e)
        {
            DeslizarSlide(btnDespesas);

            frmGerenciar_Despesas tela = new frmGerenciar_Despesas();
            OpenScreen(tela);
        }

        private void imgSettings_Click(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Tem certeza que deseja sair do programa?",
                                                  "Puro Tempero",
                                                  MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                frmLog tela = new frmLog();
                tela.Show();
                this.Hide();
            }
        }

    }
}
