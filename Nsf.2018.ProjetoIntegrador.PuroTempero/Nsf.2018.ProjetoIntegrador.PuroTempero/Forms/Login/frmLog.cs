﻿using Nsf._2018.ProjetoIntegrador.PuroTempero.DB.Módulo_de_Acesso.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero.Forms.Login
{
    public partial class frmLog : Form
    {
        public frmLog()
        {
            InitializeComponent();
            txtPassword.isPassword = false;
        }

        private void txtPassword_Click(object sender, EventArgs e)
        {
            txtPassword.isPassword = true;
        }

        private void label2_Click(object sender, EventArgs e)
        {
            frmAlterarSenha tela = new frmAlterarSenha();
            tela.Show();
            this.Hide();
        }

        private void label6_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private VIEW_AcessoDTO CarregarControles(VIEW_AcessoDTO dto)
        {
            dto.Usuario = txtUser.Text;
            dto.Senha = txtPassword.Text;

            return dto;
        }
        private VIEW_AcessoDTO VerificarUser(VIEW_AcessoDTO user)
        {
            if (user != null)
            {
                UserSession.Logado = user;

                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            else
            {
                MessageBox.Show("Credênciais inválidas!!",
                                   "Sigma",
                                   MessageBoxButtons.OK,
                                   MessageBoxIcon.Error);
            }
            return user;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            VIEW_AcessoDTO dto = new VIEW_AcessoDTO();
            CarregarControles(dto);

            VIEW_Acesso db = new VIEW_Acesso();
            VIEW_AcessoDTO user = db.Logar(dto);
            VerificarUser(user);
        }

        private void txtUser_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                VIEW_AcessoDTO dto = new VIEW_AcessoDTO();
                CarregarControles(dto);

                VIEW_Acesso db = new VIEW_Acesso();
                VIEW_AcessoDTO user = db.Logar(dto);
                VerificarUser(user);
            }
        }

        private void txtPassword_Enter(object sender, EventArgs e)
        {

            if (txtPassword.Text == "Senha")
            {
                txtPassword.isPassword = true;
                txtPassword.Text = "";
                txtPassword.ForeColor = Color.Black;
            }
        }

        private void txtPassword_Leave(object sender, EventArgs e)
        {
            if (txtPassword.Text == "")
            {
                txtPassword.isPassword = false;
                txtPassword.Text = "Senha";
                txtPassword.ForeColor = Color.Gray;
            }
        }
    }
}
