﻿using Nsf._2018.ProjetoIntegrador.PuroTempero.Forms.Login;
using Nsf._2018.ProjetoIntegrador.PuroTempero.Módulo_de_Acesso.Funcionário;
using Nsf._2018.ProjetoIntegrador.PuroTempero.User_Control.Módulo_de_Acesso.Funcionário;
using Nsf.PuroTempero.FERRAMENTAS.Email;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.ProjetoIntegrador.PuroTempero
{
    public partial class frmAlterarSenha : Form
    {
        public object Business_Funcionario { get; private set; }

        public frmAlterarSenha()
        {
            InitializeComponent();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            frmLog tela = new frmLog();
            tela.Show();
            this.Hide();
        }


        /* O que precisa ter na telinha:
         * Dois botões : Um de enviar código e outro de alterar senha 
         * : Uma com o nome de usuario, uma com o email que irá aparecer com o nome
         *  Uma para escrever o código, duas para a senha
         *  Um botãozinho para deixar a senha visivel.
         */
        private void btnTrocar_Click(object sender, EventArgs e)
        {
            TrocarSenhaEnviarEmail();

        }
        private void TrocarSenhaEnviarEmail()
        {
            Business_Funcionario business = new Business_Funcionario();
            List<DTO_Funcionario> lista2 = business.Consultar(txtUsuario.Text);
            DTO_Funcionario dto = lista2[0];
            if (lista2 == null)
            {
                MessageBox.Show("Usuario não encontrado",
                                "Puro-Tempero",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
            txtEmail.Enabled = false;
            txtEmail.Text = dto.Email;

            Emails email = new Emails();
            email.Para = txtEmail.Text;
            email.Assunto = "Troca de Senha";
            Emails_CSS emails_CSS = new Emails_CSS();
            Criacao_Codigo cod = new Criacao_Codigo();
            string codigo = cod.Codigo5();

            email.Mensagem = emails_CSS.EmailSenha(codigo);
            int id = dto.Id;

            //if (txtSenha.Text == txtNovaSenha.Text)
            //string senha = txtSenha;
            //else
            //{
            //    MessageBox.Show("Senhas não correspondem", "Puro-Tempero",
            //                      MessageBoxButtons.OK,
            //                  MessageBoxIcon.Information);
            //}

            business.TrocarSenha(id, txtUsuario.Text);
            MessageBox.Show("Senha Trocada com sucesso",
                            "Puro-Tempero",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);

        }


        //Coisa que estava no botão de trocar antes 

        // DTO_Funcionario dto = new DTO_Funcionario();
        //dto.Usuario = txtUsuario.Text;
        //
        //Database_Funcionario db = new Database_Funcionario();
        //bool verdadeiro = db.VerificarSenha(txtSenhaAtual.Text);
        //if (verdadeiro == true)
        //{
        //    ValidarSenha();
        //    dto.Senha = txtNovaSenha.Text;
        //}
        //
        //Business_Funcionario business = new Business_Funcionario();
        //business.Alterar(dto);

        //Trocando as propriedades da label
        //lblMensagem.ForeColor = Color.White;
        //lblMensagem.BackColor = Color.ForestGreen;

        //Mostrando o valor para o usuário.
        //lblMensagem.Text = "Senha Alterada com Sucesso!";




        private void lblLogin_Click(object sender, EventArgs e)
        {
            frmLog tela = new frmLog();
            tela.Show();
            this.Dispose();
        }

        private void txtUsuario_Leave(object sender, EventArgs e)
        {
            if (txtUsuario.text == "")
            {
                txtUsuario.text = "Usuário";
            }
        }

        private void txtUsuario_Enter(object sender, EventArgs e)
        {
            if (txtUsuario.text == "Usuário")
            {
                txtUsuario.text = "";
            }
        }

        private void txtEmail_Leave(object sender, EventArgs e)
        {
            if (txtEmail.text == "")
            {
                txtEmail.text = "E-Mail";
            }
        }

        private void txtEmail_Enter(object sender, EventArgs e)
        {
            if (txtEmail.text == "E-Mail")
            {
                txtEmail.text = "";
            }
        }

        private void lblLogin_Click_1(object sender, EventArgs e)
        {
            frmLog tela = new frmLog();
            tela.Show();
            this.Dispose();
        }
    }
}
    

